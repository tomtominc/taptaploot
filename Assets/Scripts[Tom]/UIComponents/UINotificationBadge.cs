﻿using UnityEngine;
using DG.Tweening;

public class UINotificationBadge : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_badge;

    public void show()
    {
        if (!gameObject.activeSelf)
        {
            gameObject.SetActive(true);

            m_badgeSequence = DOTween.Sequence();

            m_badgeSequence.Append(m_badge.DOAnchorPos((Vector2.up * 30f), 0.5f));
            m_badgeSequence.Append(m_badge.DOAnchorPos(Vector2.zero, 1f).SetEase(Ease.OutBounce));
            m_badgeSequence.AppendInterval(1f);
            m_badgeSequence.SetLoops(-1, LoopType.Restart);
        }
    }

    public void hide()
    {
        if (gameObject.activeSelf)
        {
            m_badgeSequence.Complete();
            gameObject.SetActive(false);
        }

    }

    private Sequence m_badgeSequence;
}
