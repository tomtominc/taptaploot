﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    [SerializeField]
    private float m_fingerWidth = 1f;
    [SerializeField]
    private GameObject m_tapEffect;

    private void Start()
    {
        m_controller = GameMainController.self;
        m_uiController = UIMainController.self;
    }

    private void Update()
    {
        switch (m_uiController.CurrentScreen)
        {
            case UIMainController.SCREEN.GARDEN:
                updateGardenInput();
                break;
        }
    }

    private void updateGardenInput()
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return;

#if UNITY_EDITOR
        updateGardenInputPC();

#else
        updateGardenInputMobile();
#endif
    }

    private void updateGardenInputPC()
    {
        bool tapped = false;

        if (Input.GetMouseButtonDown(0))
        {
            tapped = true;
        }

        if (Input.GetMouseButton(0))
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            touchGarden(position, tapped);
        }
    }

    private void updateGardenInputMobile()
    {
        for (int i = 0; i < Input.touches.Length; i++)
        {
            Touch touch = Input.touches[i];

            if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Stationary)
                continue;

            Vector2 position = Camera.main.ScreenToWorldPoint(touch.position);
            touchGarden(position, touch.phase == TouchPhase.Began);

        }
    }

    private void touchGarden(Vector2 p_position, bool p_tap)
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(p_position, m_fingerWidth, Vector2.zero, Mathf.Infinity);
        bool successful = hits.Length > 0;

        for (int j = 0; j < hits.Length; j++)
        {
            Transform hitObject = hits[j].transform;

            MaterialController material = hitObject.GetComponent<MaterialController>();
            AnimalController animal = hitObject.GetComponent<AnimalController>();

            if (material)
            {
                if (p_tap)
                {
                    successful = material.OnTapped();
                }
                else
                {
                    successful = material.OnHovered();
                }

            }

            if (animal)
            {
                if (p_tap)
                {
                    successful = animal.OnTapped();
                }
            }
        }

        if (p_tap && !successful)
        {
            GameObject go = Instantiate(m_tapEffect);
            go.transform.position = p_position;
            Destroy(go, 2);
        }
        else if (p_tap)
        {
            GameScreen.Instance.shake(Constants.DEFAULT_SHAKE_TIME, Constants.DEFAULT_SHAKE_FORCE);
        }
    }

    private GameMainController m_controller;
    private UIMainController m_uiController;
}
