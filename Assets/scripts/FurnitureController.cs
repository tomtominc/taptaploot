﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FurnitureController : MonoBehaviour
{
    public string resourceName;
    public string resourceTag;
    public string description;
    public Sprite sprite;
    public RecipeIngredient[] recipe;
    public int currencyRequired;
    public int experience;

    private void Start() {
        
    }

    private void Update() {
        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.y * 0.1f);
    }
}
