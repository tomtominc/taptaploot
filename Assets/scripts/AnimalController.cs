﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AnimalController : MonoBehaviour
{
    public enum STATE
    {
        NONE, IDLE, WALK, POOP, ENTER, EXIT
    }

    public static float LIFE_DELAY = 60f;
    public static float POOP_DELAY = 10f;
    public static float IDLE_DELAY = 1f;
    public static int TARGET_ANGLES = 16;
    public static float TARGET_MAX_RADIUS = 5f;
    public static float TARGET_DISTANCE = 2f;
    public static float WALK_SPEED = 1f;
    public static Vector2 POOP_POSITION = new Vector2(-1, 0);

    public string resourceTag;
    public string poopTag;
    public string produceTag;

    [SerializeField]
    private GameObject m_collectEffect;

    private STATE state;
    private float lifeTimer;
    private float stateTimer;
    private float poopTimer;
    private Vector2 walkStart;
    private Vector2 walkTarget;
    private bool produceHarvested;
    private AnimationController animationController;
    private CircleCollider2D controllerCollider;

    public CircleCollider2D getCollider()
    {
        if (controllerCollider == null) controllerCollider = GetComponent<CircleCollider2D>();
        return controllerCollider;
    }

    private void Awake()
    {
        animationController = GetComponent<AnimationController>();
    }

    void Start()
    {
        poopTimer = 0;
        lifeTimer = 0;
        produceHarvested = false;
        walkTarget = transform.localPosition;
        setState(STATE.ENTER);
    }

    void Update()
    {
        lifeTimer += Time.deltaTime;
        stateTimer += Time.deltaTime;
        poopTimer += Time.deltaTime;

        switch (state)
        {
            case STATE.IDLE:
                if (stateTimer >= IDLE_DELAY)
                {
                    if (poopTimer >= POOP_DELAY && GameMainController.self.canAddProduce() && Random.Range(0f, 1f) >= 0.5f)
                    {
                        setState(STATE.POOP);
                    }
                    else if (lifeTimer >= LIFE_DELAY)
                    {
                        setState(STATE.EXIT);
                    }
                    else
                    {
                        setState(STATE.WALK);
                    }
                }
                break;

            case STATE.WALK:
            case STATE.EXIT:
            case STATE.ENTER:
                float deltaPosition = 1f - Mathf.Clamp01((Vector2.Distance(transform.localPosition, walkTarget) - WALK_SPEED * Time.deltaTime) / Vector2.Distance(walkStart, walkTarget));
                transform.localPosition = Vector2.Lerp(walkStart, walkTarget, deltaPosition);
                if (deltaPosition >= 1)
                {
                    if (state == STATE.WALK || state == STATE.ENTER)
                    {
                        setState(STATE.IDLE);
                    }
                    else
                    {
                        Destroy(gameObject);
                    }
                }
                break;

            case STATE.POOP:
                break;
        }

        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.y * 0.1f);
    }

    public bool OnTapped()
    {
        if (!produceHarvested)
        {
            produceHarvested = true;
            GameMainController.self.spawnProduce(produceTag, (Vector2)transform.localPosition + new Vector2(POOP_POSITION.x * transform.localScale.x, POOP_POSITION.y), true);
            resetAnimation();

            if (m_collectEffect)
            {
                GameObject effect = Instantiate(m_collectEffect);
                effect.transform.position = transform.position;
                Destroy(effect, 2);
            }
            return true;
        }

        return false;
    }

    public void setAnimation(string animation)
    {
        animationController.setAnimation(animation);
    }

    public void walkTowards(Vector2 t)
    {
        walkStart = transform.localPosition;
        walkTarget = t;
        transform.localScale = (walkTarget.x < walkStart.x ? new Vector3(-1, 1, 1) : Vector3.one);
    }

    public void walkToRandomPosition()
    {
        int i;
        Vector2[] positions = new Vector2[TARGET_ANGLES];
        for (i = 0; i < positions.Length; i++)
        {
            positions[i] = (Vector2)(Quaternion.Euler(0, 0, ((float)i / TARGET_ANGLES) * 360) * Vector2.up) * TARGET_DISTANCE + (Vector2)transform.localPosition;
        }

        for (i = 0; i < positions.Length; i++)
        {
            int randomIndex = Random.Range(0, positions.Length);
            Vector2 tempVector = positions[randomIndex];
            positions[randomIndex] = positions[i];
            positions[i] = tempVector;
        }

        Vector2 newTarget = Vector2.zero;
        for (i = 0; i < positions.Length && newTarget == Vector2.zero; i++)
        {
            if (positions[i].x < TARGET_MAX_RADIUS && positions[i].x > -TARGET_MAX_RADIUS && positions[i].y < TARGET_MAX_RADIUS && positions[i].y > -TARGET_MAX_RADIUS)
            {
                newTarget = positions[i];
            }
        }
        walkTowards(newTarget);
    }

    public void resetAnimation()
    {
        switch (state)
        {
            case STATE.IDLE:
                setAnimation(produceHarvested ? "naked-idle" : "idle");
                break;

            case STATE.ENTER:
            case STATE.EXIT:
            case STATE.WALK:
                setAnimation(produceHarvested ? "naked-walk" : "walk");
                break;

            case STATE.POOP:
                setAnimation(produceHarvested ? "naked-poop" : "poop");
                break;
        }
    }

    public void setState(STATE s)
    {
        switch (s)
        {
            case STATE.ENTER:
                walkTowards(new Vector2(0, transform.localPosition.y));
                break;

            case STATE.EXIT:
                walkTowards(new Vector2(GameMainController.self.getOffscreenSize() * Mathf.Sign(transform.localPosition.x), transform.localPosition.y));
                break;

            case STATE.WALK:
                walkToRandomPosition();
                break;

            case STATE.POOP:
                poopTimer = 0;
                break;
        }

        state = s;
        stateTimer = 0;

        resetAnimation();
    }

    public void dropPoop()
    {
        GameMainController.self.spawnProduce(poopTag, (Vector2)transform.localPosition + new Vector2(POOP_POSITION.x * transform.localScale.x, POOP_POSITION.y), true);
        setState(STATE.IDLE);
    }

    public AnimalController spawn(Transform parent, float px, float py, bool animateAppearance)
    {
        AnimalController newLoot = Instantiate<GameObject>(gameObject).GetComponent<AnimalController>();
        if (newLoot == null) return null;
        newLoot.gameObject.SetActive(true);
        newLoot.transform.SetParent(parent);
        newLoot.transform.localPosition = new Vector3(px, py, py * 0.1f);
        if (animateAppearance) newLoot.setAnimation("appear");
        else newLoot.setAnimation("idle");
        return newLoot;
    }
}
