﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Panel : MonoBehaviour
{
    public virtual void open()
    {

    }

    public virtual void close()
    {

    }

    public virtual void update(float p_delta)
    {

    }
}
