﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class Utils
{
    public static string BASE64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_=";

    public static int CHAR_BITS = 6;
    public static int SLAB_BITS = 12;
    public static int CHAR_MAX_VALUE = (int)Mathf.Pow(2, CHAR_BITS) - 1;
    public static int SLAB_MAX_VALUE = (int)Mathf.Pow(2, SLAB_BITS) - 1;

    public static Color stringToColor(string str)
    {
        string hexCode = str.Replace("0x", "").Replace("#", "").ToLower();
        if (hexCode.Length >= 6)
        {
            float rr = byteToFloat(hexCode.Substring(0, 2));
            float gg = byteToFloat(hexCode.Substring(2, 2));
            float bb = byteToFloat(hexCode.Substring(4, 2));
            float aa = 1f;
            if (hexCode.Length >= 8) aa = byteToFloat(hexCode.Substring(6, 2));
            
            return new Color(rr, gg, bb, aa);
        }
        return Color.black;
    }

    public static float byteToFloat(string str)
    {
        float value = 0;
        try
        {
            value = (float)int.Parse(str, NumberStyles.HexNumber) / 255f;
        }
        catch (FormatException e)
        {
            Debug.Log(e.Message);
        }
        return value;
    }

    public static string[] splitLines(string str)
    {
        return str.Replace("\r\n","\n").Replace("\r", "\n").Split('\n');
    }

    public static string[] splitAndTrim(string str, char delimiter)
    {
        string[] parts = str.Split(delimiter);
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i] = parts[i].Trim(new char[]{' ', '\t'});
        }
        return parts;
    }

    public static GameObject findByName(GameObject[] array, string name)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (stringsMatch(array[i].name, name)) return array[i];
        }
        return null;
    }

    public static bool stringsMatch(string str1, string str2)
    {
        return str1.Equals(str2,StringComparison.CurrentCultureIgnoreCase);
    }

    public static string slabToBase64(int slab)
    {
        return "" + BASE64_CHARS[(slab & SLAB_MAX_VALUE) >> CHAR_BITS] + BASE64_CHARS[slab & CHAR_MAX_VALUE];
    }

    public static string charToBase64(int character)
    {
        return "" + BASE64_CHARS[(character & CHAR_MAX_VALUE)];
    }

    public static int base64ToSlab(string base64)
    {
        return (BASE64_CHARS.IndexOf(base64[0]) << CHAR_BITS) + BASE64_CHARS.IndexOf(base64[1]);
    }

    public static int base64ToChar(char base64)
    {
        return BASE64_CHARS.IndexOf(base64);
    }

    public static int getChecksumSlab(string code)
    {
        int checksum = 0;
        for (int i = 0; i < code.Length; i++)
        {
            checksum += code[i];
        }
        return checksum & SLAB_MAX_VALUE;
    }

    public static bool stringInList(List<string> list, string obj)
    {
        return list.Exists(delegate(string s){return s.Equals(obj);}); 
    }
}
