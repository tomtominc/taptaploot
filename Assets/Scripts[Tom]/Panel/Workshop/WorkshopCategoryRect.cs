﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkshopCategoryRect : MonoBehaviour
{
    public GameObject tabButton;
    public RectTransform containerRect;
    public RectTransform rect;

    public virtual void open()
    {
        tabButton.SetActive(false);
    }

    public virtual void close()
    {
        tabButton.SetActive(true);

        for (int i = containerRect.childCount - 1; i > -1; i--)
        {
            Destroy(containerRect.GetChild(i).gameObject);
        }
    }
}
