﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataGardenItem
{
    public string tag;
    public int cell;
    public float x;
    public float y;

    public PlayerDataGardenItem(string tag, int cell, float x, float y)
    {
        this.tag = tag;
        this.cell = cell;
        this.x = x;
        this.y = y;
    }
}
