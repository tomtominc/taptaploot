﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class GardenSelectButton : MonoBehaviour
{
    public static Vector2 PREFERRED_SIZE = new Vector2(784, 216);

    [SerializeField]
    private Text m_nameLabel;
    [SerializeField]
    private Text m_levelLabel;

    [SerializeField]
    private Button m_gardenButton;
    [SerializeField]
    private GameObject m_lockedOverlay;

    [SerializeField]
    private Slider m_gardenFilledMeter;
    [SerializeField]
    private Image m_gardenFilledMeterFill;
    [SerializeField]
    private Color m_notFilledColor;
    [SerializeField]
    private Color[] m_filledWarningColors;


    public event Action<int> onClick = delegate { };

    public void initialize(string p_name, int p_gardenIndex, PlayerDataGarden p_garden)
    {
        m_name = p_name;
        m_gardenIndex = p_gardenIndex;
        m_garden = p_garden;

        m_gardenButton.onClick.AddListener(OnClick);

        updateView();
    }

    public void updateView()
    {
        name = m_name;

        m_nameLabel.text = m_name;
        m_levelLabel.text = string.Format("Lvl {0}", m_garden.level);

        m_lockedOverlay.SetActive(m_garden.locked);
        m_gardenButton.interactable = !m_garden.locked;
    }

    public void Update()
    {
        m_gardenFilledMeter.value = GameMainController.self.getGardenFilledProgress(m_gardenIndex);

        if (m_gardenFilledMeter.value >= 1)
        {
            m_gardenFilledMeterFill.color = Color.Lerp(m_filledWarningColors[0], m_filledWarningColors[1], Mathf.PingPong(Time.time, 1));
        }
        else
        {
            m_gardenFilledMeterFill.color = m_notFilledColor;
        }

    }

    private void OnClick()
    {
        onClick(m_gardenIndex);
    }

    private string m_name;
    private int m_gardenIndex;
    private PlayerDataGarden m_garden;
}
