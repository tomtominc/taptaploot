# unique tag	name	description	sprite	recipe	currency needed	experience gained
furniture-chair-0	Chair lv.0	A simple wooden chair with a cushioned seat.	ph-furniture_3		0	0
furniture-chair-1	Chair lv.1	A simple wooden chair with a cushioned seat.	ph-furniture_2	material-wood*2, material-wool*1	2	5
furniture-chair-2	Chair lv.2	A simple wooden chair with a cushioned seat.	ph-furniture_2	material-wood*4, material-wool*2	4	10
furniture-chair-3	Chair lv.3	A simple wooden chair with a cushioned seat.	ph-furniture_2	material-wood*8, material-wool*4	8	20
furniture-table-0	Table lv.0	A small wooden table.	ph-furniture_1		0	0
furniture-table-1	Table lv.1	A small wooden table.	ph-furniture_0	material-wood*1	2	5
furniture-table-2	Table lv.2	A small wooden table.	ph-furniture_0	material-wood*2	4	10
furniture-table-3	Table lv.3	A small wooden table.	ph-furniture_0	material-wood*4	8	20