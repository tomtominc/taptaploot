﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HotelRoomSpotController : MonoBehaviour
{
    public string resourceTag;
    public string resourceName;
    public string description;
    public Vector2 roomPosition;
    public Vector2 colliderSize;
    public Vector2 colliderOffset;
    public string[] furniture;
    public int index;
    
    private int level;

    void Start ()
    {
        
	}
	
	void Update ()
    {
		
	}

    public void OnMouseUp() {
        if (!EventSystem.current.IsPointerOverGameObject()) UIMainController.self.onClickHotelRoomSpot(UIMainController.self.cameraMain.WorldToViewportPoint(transform.position), index);
    }
    
    public void remove()
    {
        Destroy(gameObject);
    }

    public string getCurrentFurnitureTag()
    {
        return furniture[level];
    }

    public FurnitureController getCurrentFurniture()
    {
        return Constants.furniture[furniture[level]];
    }
    
    public void setLevel(int l)
    {
        level = l;
        GetComponent<SpriteRenderer>().sprite = getCurrentFurniture().sprite;
    }
}
