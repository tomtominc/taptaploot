﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MaterialController : MonoBehaviour
{
    public enum TYPE
    {
        NONE, CURRENCY, MATERIAL, SPAWNABLE
    }

    public TYPE type;
    public string resourceTag;
    public string resourceName;
    public string description;
    public Sprite sprite;
    public int amount;
    public int experience;
    public bool canCollectOnHover;
    public bool isIdle;

    public SpawnDefinition[] objectSpawnDefitions;

    [SerializeField]
    private GameObject m_collectEffect;

    private int gridIndex;
    private bool wasTouched;
    private AnimationController animationController;
    private CircleCollider2D controllerCollider;

    public CircleCollider2D getCollider()
    {
        if (controllerCollider == null) controllerCollider = GetComponent<CircleCollider2D>();
        return controllerCollider;
    }

    void Start()
    {
        wasTouched = false;
        isIdle = false;
    }

    void Update()
    {

    }

    public bool OnTapped()
    {
        if (isIdle && !wasTouched)
        {
            collect();
            return true;
        }

        return false;
    }

    public bool OnHovered()
    {
        if (canCollectOnHover && isIdle && !wasTouched)
        {
            collect();
            return true;
        }

        return false;
    }

    public void remove()
    {
        Destroy(gameObject);
    }

    public void setGridIndex(int i)
    {
        gridIndex = i;
    }

    public void collect()
    {
        if (objectSpawnDefitions != null && objectSpawnDefitions.Length > 0)
        {
            SpawnDefinition randomObject = SpawnDefinition.getRandomObject(objectSpawnDefitions);
            MaterialController objectController = randomObject.spawnMaterial(transform.parent, transform.localPosition.x, transform.localPosition.y, true);
            if (objectController != null)
            {
                GameMainController.self.setGridObject(objectController, gridIndex);
            }
        }
        GameMainController.self.addMaterial(resourceTag, amount);
        GameMainController.self.addGardenExperience(experience);
        setAnimation("disappear");
        wasTouched = true;

        if (m_collectEffect != null)
        {
            GameObject collectEffect = Instantiate(m_collectEffect);
            collectEffect.transform.position = transform.position;
            Destroy(collectEffect, 3);
        }

    }

    public MaterialController spawn(Transform parent, float px, float py, bool animateAppearance)
    {
        MaterialController newLoot = Instantiate<GameObject>(gameObject).GetComponent<MaterialController>();
        if (newLoot == null) return null;
        newLoot.gameObject.SetActive(true);
        newLoot.transform.SetParent(parent);
        newLoot.transform.localPosition = new Vector3(px, py, py * 0.1f);
        newLoot.onSpawned();
        if (animateAppearance) newLoot.setAnimation("appear");
        else newLoot.setAnimation("idle");
        return newLoot;
    }

    public void setAnimation(string animation)
    {
        getAnimationController().setAnimation(animation);
    }

    public AnimationController getAnimationController()
    {
        if (animationController == null) animationController = GetComponent<AnimationController>();
        return animationController;
    }

    public virtual void onSpawned()
    {

    }
}
