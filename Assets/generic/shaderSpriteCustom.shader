﻿// Custom Sprite Shader
// written by David Jalbert from Unity3D's default sprite shader
// Last update: 2018-01-27

Shader "Custom/SpriteCustom"
{
    Properties
    {
        [PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
        
        [MaterialToggle] _Invert("Invert", Float) = 0
        _Hue("Hue", Range(0, 359)) = 0
        _Saturation("Saturation", Range(-1, 1)) = 0
        _Lightness("Lightness", Range(-1, 1)) = 0
        _Contrast("Contrast", Range(-1, 10)) = 0
        [MaterialToggle] _Colorize("Colorize", Range(0, 1)) = 0
        _ColorizeHue("Colorize Hue", Range(0, 359)) = 0
        _Tint("Tint", Color) = (1,1,1,1)
        _TintAmount("Tint Amount", Range(0, 1)) = 0
        [MaterialToggle] PixelSnap("Pixel Snap", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "IgnoreProjector" = "True"
            "RenderType" = "Transparent"
            "PreviewType" = "Plane"
            "CanUseSpriteAtlas" = "True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color : COLOR;
                float2 texcoord  : TEXCOORD0;
            };

            struct Hsl
            {
                fixed h;
                fixed s;
                fixed l;
                fixed a;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap(OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _MainTex;
            sampler2D _AlphaTex;
            float _AlphaSplitEnabled;

            fixed4 SampleSpriteTexture(float2 uv)
            {
                fixed4 color = tex2D(_MainTex, uv);

                #if UNITY_TEXTURE_ALPHASPLIT_ALLOWED
                if (_AlphaSplitEnabled)
                    color.a = tex2D(_AlphaTex, uv).r;
                #endif //UNITY_TEXTURE_ALPHASPLIT_ALLOWED

                return color;
            }

            float _Contrast;
            float _Hue;
            float _Saturation;
            float _Lightness;
            float _Invert;
            fixed4 _Tint;
            float _TintAmount;
            float _Colorize;
            float _ColorizeHue;

            Hsl rgbToHsl(fixed4 c)
            {
                fixed cMax = max(max(c.r, c.g), c.b);
                fixed cMin = min(min(c.r, c.g), c.b);
                fixed delta = cMax - cMin;
                Hsl hsl;
                hsl.h = 0;
                hsl.s = 0;
                hsl.l = (cMax + cMin) / 2;
                hsl.a = c.a;

                if (delta == 0)
                {
                    hsl.h = 0;
                }
                else if (cMax == c.r)
                {
                    hsl.h = 60 * (((c.g - c.b) / delta) + 6);
                }
                else if (cMax == c.g)
                {
                    hsl.h = 60 * (((c.b - c.r) / delta) + 2);
                }
                else
                {
                    hsl.h = 60 * (((c.r - c.g) / delta) + 4);
                }

                hsl.h = hsl.h % 360;

                if (delta == 0)
                {
                    hsl.s = 0;
                }
                else
                {
                    hsl.s = delta / (1 - abs(2 * hsl.l - 1));
                }
                
                return hsl;
            }

            fixed4 hslToRgb(Hsl hsl)
            {
                fixed4 c;

                fixed cPrimary = (1 - abs(2 * hsl.l - 1)) * hsl.s;
                fixed cSecondary = cPrimary * (1 - abs((hsl.h / 60) % 2 - 1));
                fixed m = hsl.l - cPrimary / 2;

                if (hsl.h < 60)
                {
                    c.r = cPrimary;
                    c.g = cSecondary;
                    c.b = 0;
                }
                else if (hsl.h < 120)
                {
                    c.r = cSecondary;
                    c.g = cPrimary;
                    c.b = 0;
                }
                else if (hsl.h < 180)
                {
                    c.r = 0;
                    c.g = cPrimary;
                    c.b = cSecondary;
                }
                else if (hsl.h < 240)
                {
                    c.r = 0;
                    c.g = cSecondary;
                    c.b = cPrimary;
                }
                else if (hsl.h < 300)
                {
                    c.r = cSecondary;
                    c.g = 0;
                    c.b = cPrimary;
                }
                else if (hsl.h < 360)
                {
                    c.r = cPrimary;
                    c.g = 0;
                    c.b = cSecondary;
                }

                c.rgb += m;
                c.a = hsl.a;

                return c;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 c = SampleSpriteTexture(IN.texcoord) * IN.color;
                Hsl hslC, hslTint;

                // invert
                if (_Invert == 1) c.rgb = (1 - c.rgb);

                // HSL
                if (_Hue != 0 || _Saturation != 0 || _Lightness != 0)
                {
                    hslC = rgbToHsl(c);
                    hslC.h = (hslC.h + _Hue) % 360;
                    hslC.s = max(0, hslC.s + _Saturation);
                    hslC.l = min(1, max(0, hslC.l + _Lightness));
                    c = hslToRgb(hslC);
                }

                // contrast
                if (_Contrast != 0)
                {
                    c.r = min(1, max(0, ((_Contrast + 1) * (c.r - 0.5) + 0.5)));
                    c.g = min(1, max(0, ((_Contrast + 1) * (c.g - 0.5) + 0.5)));
                    c.b = min(1, max(0, ((_Contrast + 1) * (c.b - 0.5) + 0.5)));
                }

                // colorize
                if (_Colorize != 0)
                {
                    hslC = rgbToHsl(c);
                    hslC.h = _ColorizeHue;
                    c = hslToRgb(hslC);
                }

                // tint
                if (_TintAmount != 0) c.rgb = (1 - _TintAmount) * c.rgb + _TintAmount * _Tint;

                c.rgb *= c.a;
                return c;
            }
            ENDCG
        }
    }
}