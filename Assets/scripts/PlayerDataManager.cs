﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class PlayerDataManager
{
    public static string RELATIVE_PATH = "/savedata.dat";

    public static PlayerData data;

    public static void load()
    {
        if (File.Exists(Application.persistentDataPath + RELATIVE_PATH))
        {
            BinaryFormatter bf = new BinaryFormatter ();
            FileStream file = File.Open(Application.persistentDataPath + RELATIVE_PATH, FileMode.Open);
            try
            {
                setData((PlayerData)bf.Deserialize(file));
            }
            catch (SerializationException e)
            {
                Debug.Log(e.StackTrace);
                clear();
            }
            file.Close ();
        }
        else clear();
    }

    public static void clear()
    {
        setData(createNewData());
    }

    public static void save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + RELATIVE_PATH);
        bf.Serialize(file, getData());
        file.Close ();
    }

    public static PlayerData getData()
    {
        if (data == null) load();
        return data;
    }

    public static void setData(PlayerData d)
    {
        data = d;
    }

    public static PlayerData createNewData()
    {
        PlayerData newData = new PlayerData();

        switch (Application.systemLanguage)
        {
        case SystemLanguage.French: newData.language = 1; break;
        default: newData.language = 0; break;
        }
        newData.needHelpShown = false;
        newData.currency = 0;
        newData.premiumCurrency = 0;
        newData.inventoryItems = new Dictionary<string, PlayerDataInventoryItem>();
        newData.hotelExperience = 0;
        newData.hotelLevel = 0;

        newData.gardens = new List<PlayerDataGarden>();
        for (int i = 0; i < Constants.NUM_GARDENS; i++)
        {
            PlayerDataGarden newGarden = new PlayerDataGarden();
            newGarden.gardenItems = new List<PlayerDataGardenItem>();
            newGarden.produceItems = new List<PlayerDataProduceItem>();
            newGarden.lastPlayTime = System.DateTime.Now;
            newGarden.experience = 0;
            newGarden.level = 0;
            newGarden.locked = true;
            newGarden.objectsToSpawn = 0;
            newData.gardens.Add(newGarden);
        }

        newData.hotelrooms = new List<PlayerDataHotelRoom>();
        for (int i = 0; i < Constants.NUM_HOTEL_ROOMS; i++)
        {
            PlayerDataHotelRoom newHotelRoom = new PlayerDataHotelRoom();
            newHotelRoom.locked = true;
            newHotelRoom.isBuilding = false;
            newHotelRoom.buildTime = System.DateTime.Now;
            newHotelRoom.cobwebsCleared = false;
            newHotelRoom.spots = new PlayerDataHotelRoomSpot[Constants.NUM_ROOM_SPOTS];
            for (int j = 0; j < Constants.NUM_ROOM_SPOTS; j++)
            {
                newHotelRoom.spots[j] = new PlayerDataHotelRoomSpot();
                newHotelRoom.spots[j].level = 0;
                newHotelRoom.spots[j].buildTime = System.DateTime.Now;
                newHotelRoom.spots[j].isBuilding = false;
            }
            newData.hotelrooms.Add(newHotelRoom);
        }
        
        newData.gardens[0].locked = false;
        newData.gardens[0].objectsToSpawn = Constants.GARDEN_START_OBJECTS;
        newData.hotelrooms[0].locked = false;
        newData.lastGarden = 0;
        

        return newData;
    }
}