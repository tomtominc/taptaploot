﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataInventoryItem
{
    public enum TYPE
    {
        NONE, MATERIAL, FURNITURE, BLUEPRINT
    }

    public TYPE type;
    public string tag;
    public int count;

    public PlayerDataInventoryItem()
    {
        this.type = TYPE.NONE;
        this.tag = "";
        this.count = 0;
    }

    public PlayerDataInventoryItem(TYPE type, string tag, int count)
    {
        this.type = type;
        this.tag = tag;
        this.count = count;
    }
}
