﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ItemPopup : Panel
{
    [SerializeField]
    private CanvasGroup m_canvasGroup;
    [SerializeField]
    private RectTransform m_popupRect;
    [SerializeField]
    private Image m_itemIcon;
    [SerializeField]
    private Text m_itemDescription;
    [SerializeField]
    private Text m_itemAmount;
    [SerializeField]
    private Text m_itemName;

    public override void open()
    {
        gameObject.SetActive(true);

        m_canvasGroup.alpha = 0;
        m_canvasGroup.DOFade(1f, 0.5f);
        m_popupRect.localScale = Vector3.zero;
        m_popupRect.DOScale(1f, 0.5f).SetEase(Ease.OutBack);
    }

    public override void close()
    {
        if (gameObject.activeSelf)
        {
            m_canvasGroup.DOFade(0f, 0.5f);
            m_popupRect.DOScale(0f, 0.5f).SetEase(Ease.InBack)
                .OnComplete(() => gameObject.SetActive(false));
        }
    }

    public void setItem(PlayerDataInventoryItem p_item)
    {
        MaterialController material = Constants.materials[p_item.tag];
        m_itemName.text = material.resourceName;
        m_itemDescription.text = material.description;
        m_itemAmount.text = p_item.count.ToString();
        m_itemIcon.sprite = material.sprite;
    }

}
