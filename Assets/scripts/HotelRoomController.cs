﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotelRoomController : MonoBehaviour
{
    public int index;
    public string resourceTag;
    public string resourceName;
    public string description;
    public string background;
    public RecipeIngredient[] recipe;
    public int currencyRequired;
    public string[] spots;
    public Vector2 maskSize;
    public Vector2 maskOffset;

    void Start()
    {

    }

    void Update()
    {

    }
}
