﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SpawnDefinition
{
    public string tag;
    public float spawnProbability;

    public MaterialController spawnMaterial(Transform parent, float px, float py, bool animateAppearance)
    {
        MaterialController objectController = Constants.getMaterialController(tag);
        if (objectController != null) return objectController.spawn(parent, px, py, animateAppearance);
        return null;
    }

    public AnimalController spawnAnimal(Transform parent, float px, float py, bool animateAppearance)
    {
        AnimalController objectController = Constants.getAnimalController(tag);
        if (objectController != null) return objectController.spawn(parent, px, py, animateAppearance);
        return null;
    }

    public static int getRandomObjectIndex(SpawnDefinition[] objects)
    {
        if (objects == null || objects.Length <= 0) return -1;

        int i;
        float[] spawnProbabilities = new float[objects.Length];
        float spawnDenominator = 0;

        for (i = 0; i < objects.Length; i++)
        {
            spawnDenominator += objects[i].spawnProbability;
            spawnProbabilities[i] = spawnDenominator;
        }

        float spawnNumerator = Random.Range(0, spawnDenominator);
        int spawnIndex = -1;
        for (i = 0; i < spawnProbabilities.Length && spawnIndex < 0; i++)
        {
            if (spawnNumerator <= spawnProbabilities[i]) spawnIndex = i;
        }
        return spawnIndex;
    }

    public static SpawnDefinition getRandomObject(SpawnDefinition[] objects)
    {
        if (objects == null || objects.Length <= 0) return null;
        return objects[getRandomObjectIndex(objects)];
    }
}
