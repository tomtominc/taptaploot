﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataProduceItem
{
    public string tag;
    public float x;
    public float y;

    public PlayerDataProduceItem(string tag, float x, float y)
    {
        this.tag = tag;
        this.x = x;
        this.y = y;
    }
}
