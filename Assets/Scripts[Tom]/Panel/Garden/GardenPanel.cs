﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class GardenPanel : Panel
{
    public Slider m_gardenExperience;
    public RectTransform m_gardenLevelLayout;
    public RectTransform m_gardenContainer;
    public List<GameObject> m_gardenPrefabs;

    public PlayerDataGarden garden
    {
        get { return GameMainController.self.getCurrentGarden(); }
    }

    public override void open()
    {
        m_gardenContainer.gameObject.SetActive(true);
        m_gardenExperience.gameObject.SetActive(true);
    }

    public override void close()
    {
        m_gardenContainer.gameObject.SetActive(false);
        m_gardenExperience.gameObject.SetActive(false);
    }

    public void OnChangeGarden()
    {
        for (int i = m_gardenContainer.childCount - 1; i > -1; i--)
        {
            Destroy(m_gardenContainer.GetChild(i).gameObject);
        }

        int gardenIndex = GameMainController.self.getCurrentGardenIndex();

        GameObject gardenPrefab = m_gardenPrefabs[gardenIndex];
        GameObject gardenObj = Instantiate(gardenPrefab);
        gardenObj.transform.SetParent(m_gardenContainer, false);

        for (int i = 0; i < m_gardenLevelLayout.childCount; i++)
        {
            m_gardenLevelLayout.GetChild(i).gameObject.SetActive(i < garden.level);
        }
    }

    public void OnGardenLevelUp()
    {
        for (int i = 0; i < m_gardenLevelLayout.childCount; i++)
        {
            m_gardenLevelLayout.GetChild(i).gameObject.SetActive(i < garden.level);
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            int nextGarden = GameMainController.self.getCurrentGardenIndex() + 1;
            if (nextGarden > 4)
            {
                nextGarden = 0;
            }

            GameMainController.self.setGarden(nextGarden);

            OnChangeGarden();
        }

        m_gardenExperience.value = getGardenExperiencePercent();
    }

    private float getGardenExperiencePercent()
    {
        return (float)garden.experience / (float)Constants.getGardenExperience(garden.level);
    }
}
