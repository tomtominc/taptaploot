﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class NavigationTab : MonoBehaviour
{
    [SerializeField]
    private int m_tabIndex;
    [SerializeField]
    private Button m_tabButton;
    [SerializeField]
    private Toggle m_tabToggle;
    [SerializeField]
    private UINotificationBadge m_notificationBadge;

    public event Action<int> clickedTabAction = delegate { };

    private void OnEnable()
    {
        m_tabButton.onClick.AddListener(OnClickedTab);
    }

    private void OnDisable()
    {
        m_tabButton.onClick.RemoveListener(OnClickedTab);
    }

    public void showNotificationBadge()
    {
        m_notificationBadge.show();
    }

    public void hideNotificationBadge()
    {
        m_notificationBadge.hide();
    }


    private void OnClickedTab()
    {
        m_tabToggle.isOn = true;
        clickedTabAction(m_tabIndex);
    }
}
