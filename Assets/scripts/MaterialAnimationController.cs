﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialAnimationController : MonoBehaviour
{
    public MaterialController lootController;

    public void onAnimationEnd()
    {
        lootController.remove();
    }

    public void onTurnIdle()
    {
        lootController.isIdle = true;
    }
}
