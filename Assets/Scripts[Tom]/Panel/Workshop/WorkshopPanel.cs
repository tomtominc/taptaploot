﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkshopPanel : Panel
{
    [SerializeField]
    private RectTransform m_inventoryItemPrefab;
    [SerializeField]
    private List<WorkshopCategoryRect> m_categoryRects;

    public override void open()
    {
        gameObject.SetActive(true);
    }

    public override void close()
    {
        gameObject.SetActive(false);
    }

    public void setTab(int p_tabCategory)
    {
        for (int i = 0; i < m_categoryRects.Count; i++)
        {
            m_categoryRects[i].close();
        }

        WorkshopCategoryRect categoryRect = m_categoryRects[p_tabCategory];
        categoryRect.rect.SetSiblingIndex(transform.childCount - 1);
        categoryRect.open();

        switch (p_tabCategory)
        {
            case 0:
                foreach (var kvp in PlayerDataManager.getData().inventoryItems)
                {
                    if (kvp.Value.count > 0)
                    {
                        RectTransform inventoryItemRect = Instantiate(m_inventoryItemPrefab);
                        InventoryButton inventoryItem = inventoryItemRect.GetComponent<InventoryButton>();
                        inventoryItem.setItem(kvp.Value);
                        inventoryItem.transform.SetParent(categoryRect.containerRect, false);
                    }
                }
                break;
            case 1:

                break;
        }

    }
}
