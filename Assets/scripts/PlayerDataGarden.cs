﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataGarden
{
    public List<PlayerDataGardenItem> gardenItems;
    public List<PlayerDataProduceItem> produceItems;
    public System.DateTime lastPlayTime;
    public int experience;
    public int level;
    public bool locked;
    public int objectsToSpawn;
}
