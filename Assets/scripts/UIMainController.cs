﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMainController : MonoBehaviour
{
    public const int TAB_MATERIAL = 0;
    public const int TAB_FURNITURE = 1;
    public const int TAB_BLUEPRINT = 2;

    public static Vector2 LOW_UPGRADE_POSITION = new Vector2(0, 320);
    public static Vector2 HIGH_UPGRADE_POSITION = new Vector2(0, -320);

    public enum SCREEN
    {
        NONE, WORKSHOP, GARDEN, GARDEN_MAP, HOTEL, HOTEL_MAP
    }

    public static UIMainController self;

    public Camera cameraMain;
    public Text textSpawnTimer;
    public Text textCurrency;
    public Text textGardenLevel;
    public Text textHotelLevel;
    public RectTransform rectGardenLevelMeter;
    public RectTransform rectGardenLevelMeterBg;
    public RectTransform rectHotelLevelMeter;
    public RectTransform rectHotelLevelMeterBg;
    public GameObject panelGardenUnlocked;
    public Text textGardenUnlockedName;

    [Space(10)]
    public Transform containerGarden;
    public Transform containerWorkshop;
    public Transform containerHotel;

    [Space(10)]
    public GameObject panelInventory;
    public GameObject panelInventoryList;
    public GameObject panelInventoryItem;
    public Text textInventoryItemName;
    public Text textInventoryItemDescription;
    public Text textInventoryItemAmount;
    public Image imageInventoryItem;
    public GameObject inventoryContainer;

    [Space(10)]
    public GameObject panelHotelRoomTopUI;
    public GameObject panelHotelRoomRecipe;
    public Button buttonHotelRoomBuild;
    public Text textHotelRoomName;
    public GameObject[] objectHotelRoomIngredients;
    public Image[] imageHotelRoomIngredientSprites;
    public Text[] textHotelRoomIngredientNames;
    public Text[] textHotelRoomIngredientNeeded;
    public Text[] textHotelRoomIngredientOwned;

    [Space(10)]
    public GameObject panelHotelMap;
    public Text[] labelHotelRooms;

    [Space(10)]
    public SpriteRenderer spriteHotelRoomBackground;
    public GameObject panelFurnitureRecipe;
    public Text textFurnitureName;
    public Button buttonFurnitureBuild;
    public GameObject[] objectFurnitureIngredients;
    public Image[] imageFurnitureIngredientSprites;
    public Text[] textFurnitureIngredientNeeded;

    [Space(10)]
    public GameObject panelGardenMap;
    public CustomToggle[] toggleMapGardens;

    [Space(10)]
    public Toggle toggleInventoryTabResources;
    public Toggle toggleInventoryTabFurniture;
    public Toggle toggleInventoryTabBlueprints;
    public Image imageDayTimer;
    public GameObject prefabInventoryButton;

    [Space(10)]
    [Header("Panels")]
    public WorkshopPanel workshopPanel;
    public GardenSelectionPanel gardenSelectionPanel;
    public NavigationBar navigationBar;
    public GardenPanel gardenPanel;
    public HotelRoomPanel hotelRoomPanel;

    [Space(10)]
    [Header("Popups")]
    public ItemPopup itemPopup;
    public FurnitureRecipePopup furniturePopup;



    private List<InventoryButton> inventoryButtons;
    private SCREEN currentScreen;

    public SCREEN CurrentScreen
    {
        get { return currentScreen; }
    }

    void Awake()
    {
        self = this;
        inventoryButtons = new List<InventoryButton>();
    }

    void Start()
    {
        setCurrentScreen(SCREEN.GARDEN);
    }

    void Update()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float clampedAspect = Mathf.Clamp(screenAspect, Constants.ASPECT_MIN, Constants.ASPECT_MAX);

        cameraMain.orthographicSize = Mathf.Lerp(Constants.CAMERA_MIN, Constants.CAMERA_MAX, 1f - (clampedAspect - Constants.ASPECT_MIN) / (Constants.ASPECT_MAX - Constants.ASPECT_MIN));

        switch (currentScreen)
        {
            case SCREEN.GARDEN:
                break;

            case SCREEN.HOTEL:
                rectHotelLevelMeter.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Lerp(Constants.LEVEL_METER_MIN_SIZE, rectHotelLevelMeterBg.rect.width, (float)PlayerDataManager.getData().hotelExperience / (float)Constants.getHotelExperience(PlayerDataManager.getData().hotelLevel)));
                textHotelLevel.text = "Lvl. " + PlayerDataManager.getData().hotelLevel.ToString() + "\n" + Constants.hotelRooms[GameMainController.self.getCurrentHotelIndex()].resourceName;
                break;
        }
    }

    public void setCurrentScreen(SCREEN s)
    {
        if (currentScreen != s)
        {
            panelHotelRoomRecipe.SetActive(false);
            panelHotelMap.SetActive(false);

            workshopPanel.close();
            furniturePopup.close();
            hotelRoomPanel.close();

            if (s == SCREEN.GARDEN)
            {
                gardenSelectionPanel.close();
            }
            else
            {
                gardenSelectionPanel.gameObject.SetActive(false);
            }

            if (s != SCREEN.GARDEN_MAP)
            {
                gardenPanel.close();
            }

            currentScreen = s;
            switch (currentScreen)
            {
                case SCREEN.WORKSHOP:
                    workshopPanel.open();
                    workshopPanel.setTab(0);
                    break;
                case SCREEN.HOTEL_MAP:
                    panelHotelMap.SetActive(true);
                    for (int i = 0; i < labelHotelRooms.Length; i++)
                    {
                        Text roomLabel = labelHotelRooms[i];
                        roomLabel.text = Constants.hotelRooms[i].resourceName;
                        if (PlayerDataManager.getData().hotelrooms[i].locked)
                        {
                            roomLabel.text += " (locked)";
                        }
                    }
                    break;
                case SCREEN.GARDEN:
                    gardenPanel.open();
                    gardenPanel.OnChangeGarden();
                    cameraMain.transform.position = new Vector3(containerGarden.position.x, containerGarden.position.y, Constants.CAMERA_Z);
                    break;
                case SCREEN.GARDEN_MAP:
                    gardenPanel.open();
                    gardenSelectionPanel.open();
                    break;
                case SCREEN.HOTEL:
                    // panelHotelRoomTopUI.SetActive(true);
                    cameraMain.transform.position = new Vector3(containerHotel.position.x, containerHotel.position.y, Constants.CAMERA_Z);
                    //spriteHotelRoomBackground.sprite = Constants.getSprite(Constants.hotelRooms[GameMainController.self.getCurrentHotelIndex()].background);
                    hotelRoomPanel.open();
                    break;

            }
        }
    }

    public void onClickInventory()
    {
        setCurrentScreen(SCREEN.WORKSHOP);
    }

    public void onClickMap()
    {
        if (currentScreen != SCREEN.GARDEN_MAP)
        {
            setCurrentScreen(SCREEN.GARDEN_MAP);
        }
    }

    public void onClickMapGarden(int i)
    {
        if (currentScreen == SCREEN.GARDEN_MAP)
        {
            GameMainController.self.setGarden(i);
            setCurrentScreen(SCREEN.GARDEN);

        }
    }

    public void onClickHotelRoomSpot(Vector2 objectPosition, int i)
    {
        if (currentScreen == SCREEN.HOTEL)
        {
            GameMainController.self.setSelectedHotelRoomSpotIndex(i);
            showFurnitureRecipe(objectPosition);
        }
    }

    public void onClickHotelRoom(Vector2 objectPosition, int i)
    {
        if (currentScreen == SCREEN.HOTEL_MAP)
        {
            GameMainController.self.setSelectedHotelRoomIndex(i);
            if (PlayerDataManager.getData().hotelrooms[i].locked)
            {
                showHotelRoomRecipe(objectPosition);
            }
            else
            {
                GameMainController.self.setHotelRoom(i);
                setCurrentScreen(SCREEN.HOTEL);
            }
        }
    }

    public void onClickHotelRoomRecipeClose()
    {
        panelHotelRoomRecipe.SetActive(false);
    }

    public void onClickFurnitureRecipeClose()
    {
        panelFurnitureRecipe.SetActive(false);
    }

    public void onUnlockedGarden(int p_gardenIndex)
    {
        //navigationBar.showNotificationIcon(1);
    }

    public void onGardenLevelUp(int p_gardenIndex)
    {
        //gardenPanel.OnGardenLevelUp();
    }

    public void onClickHotelRoomBuild()
    {
        if (GameMainController.self.hasSelectedHotelRoomRecipeItems() && GameMainController.self.hasSelectedHotelRoomCurrency())
        {
            GameMainController.self.unlockSelectedHotelRoom();
            setCurrentScreen(SCREEN.HOTEL);
        }
    }

    public void onClickFurnitureBuild()
    {
        if (GameMainController.self.hasNextSelectedFurnitureRecipeItems() && GameMainController.self.hasNextSelectedFurnitureCurrency())
        {
            GameMainController.self.unlockNextSelectedFurniture();
            furniturePopup.close();
        }
    }

    public void showFurnitureRecipe(Vector2 objectPosition)
    {
        Vector2 anchorPosition = objectPosition;

        if (objectPosition.y > 0)
        {
            anchorPosition = HIGH_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        }
        else
        {
            anchorPosition = LOW_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        }

        FurnitureController furnitureUpgrade = GameMainController.self.getNextSelectedFurnitureUpgrade();
        FurnitureController currentFurniture = GameMainController.self.getCurrentSelectedFurniture();
        furniturePopup.initialize(currentFurniture, furnitureUpgrade, anchorPosition);
        furniturePopup.open();

        //panelFurnitureRecipe.SetActive(true);
        //if (objectPosition.y > 0) panelFurnitureRecipe.GetComponent<RectTransform>().anchoredPosition = HIGH_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        //else panelFurnitureRecipe.GetComponent<RectTransform>().anchoredPosition = LOW_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        //FurnitureController furnitureUpgrade = getNextFurnitureUpgrade(selectedHotelRoomIndex, selectedHotelRoomSpotIndex);
        //textFurnitureName.text = getCurrentFurniture(selectedHotelRoomIndex, selectedHotelRoomSpotIndex).resourceName;
        //if (furnitureUpgrade == null)
        //{
        //    buttonFurnitureBuild.interactable = false;
        //    buttonFurnitureBuild.GetComponent<ButtonLabel>().label.text = "Maxed";
        //    for (int j = 0; j < objectFurnitureIngredients.Length; j++)
        //    {
        //        objectFurnitureIngredients[j].SetActive(false);
        //    }
        //}
        //else
        //{
        //    bool enoughIngredients = hasRecipeItems(furnitureUpgrade.recipe);
        //    buttonFurnitureBuild.interactable = enoughIngredients;
        //    buttonFurnitureBuild.GetComponent<ButtonLabel>().label.text = enoughIngredients ? "Upgrade" : "Not enough materials";
        //    for (int j = 0; j < objectFurnitureIngredients.Length; j++)
        //    {
        //        if (j < furnitureUpgrade.recipe.Length)
        //        {
        //            objectFurnitureIngredients[j].SetActive(true);
        //            RecipeIngredient ingredient = furnitureUpgrade.recipe[j];
        //            MaterialController material = Constants.materials[ingredient.resourceTag];
        //            imageFurnitureIngredientSprites[j].sprite = material.sprite;
        //            textFurnitureIngredientNeeded[j].text = ingredient.amount.ToString();
        //            bool hasIngredient = PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag) ? PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count >= ingredient.amount : false;
        //            if (!hasIngredient) textFurnitureIngredientNeeded[j].color = Color.red;
        //            else textFurnitureIngredientNeeded[j].color = Color.white;
        //        }
        //        else
        //        {
        //            objectFurnitureIngredients[j].SetActive(false);
        //        }
        //    }
        //}
    }

    public void showHotelRoomRecipe(Vector2 objectPosition)
    {
        panelHotelRoomRecipe.SetActive(true);
        if (objectPosition.y > 0) panelHotelRoomRecipe.GetComponent<RectTransform>().anchoredPosition = HIGH_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        else panelHotelRoomRecipe.GetComponent<RectTransform>().anchoredPosition = LOW_UPGRADE_POSITION + Vector2.up * objectPosition.y;
        //textHotelRoomName.text = Constants.hotelRooms[selectedHotelRoomIndex].resourceName;
        //bool enoughIngredients = hasRecipeItems(Constants.hotelRooms[selectedHotelRoomIndex].recipe);
        textHotelRoomName.text = GameMainController.self.getSelectedHotelRoomController().resourceName;
        bool enoughIngredients = GameMainController.self.hasSelectedHotelRoomRecipeItems();
        buttonHotelRoomBuild.interactable = enoughIngredients;
        buttonHotelRoomBuild.GetComponent<ButtonLabel>().label.text = enoughIngredients ? "Unlock" : "Not enough materials";
        for (int j = 0; j < objectHotelRoomIngredients.Length; j++)
        {
            if (j < GameMainController.self.getSelectedHotelRoomController().recipe.Length)
            {
                objectHotelRoomIngredients[j].SetActive(true);
                RecipeIngredient ingredient = GameMainController.self.getSelectedHotelRoomController().recipe[j];
                MaterialController material = Constants.materials[ingredient.resourceTag];
                imageHotelRoomIngredientSprites[j].sprite = material.sprite;
                //textHotelRoomIngredientNames[j].text  = material.resourceName;
                textHotelRoomIngredientNeeded[j].text = ingredient.amount.ToString();
                bool hasIngredient = PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag) ? PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count >= ingredient.amount : false;
                if (!hasIngredient) textHotelRoomIngredientNeeded[j].color = Color.red;
                else textHotelRoomIngredientNeeded[j].color = Color.white;
                //textHotelRoomIngredientOwned[j].text  = PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag) ? PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count.ToString() : "0";
            }
            else
            {
                objectHotelRoomIngredients[j].SetActive(false);
            }
        }
    }

    public void onClickHotel()
    {
        if (currentScreen != SCREEN.HOTEL_MAP)
        {
            setCurrentScreen(SCREEN.HOTEL_MAP);
        }
    }

    public void onClickInventoryTab(int tab)
    {
        //rebuildInventory(tab, inventoryContainer);
    }

    public void onClickNextGarden(bool response)
    {
        if (response)
        {
            GameMainController.self.setGarden(GameMainController.self.getCurrentGardenIndex() + 1);
        }
        panelGardenUnlocked.SetActive(false);
    }

    public void rebuildInventory(int tab, GameObject container)
    {
        int i;

        for (i = 0; i < inventoryButtons.Count; i++)
        {
            Destroy(inventoryButtons[i].gameObject);
        }
        inventoryButtons.Clear();

        switch (tab)
        {
            case 0:
                foreach (KeyValuePair<string, PlayerDataInventoryItem> kvp in PlayerDataManager.getData().inventoryItems)
                {
                    if (kvp.Value.count > 0)
                    {
                        InventoryButton newButton = Instantiate<GameObject>(prefabInventoryButton).GetComponent<InventoryButton>();
                        newButton.setItem(kvp.Value);
                        newButton.transform.SetParent(container.transform);
                        newButton.transform.localScale = Vector3.one;
                        newButton.transform.localPosition = Vector3.zero;
                        inventoryButtons.Add(newButton);
                    }
                }
                break;

            case 2:
                break;
        }

        for (i = 0; i < inventoryButtons.Count; i++)
        {
            int cellColumn = i % Constants.INVENTORY_COLUMNS;
            int cellRow = i / Constants.INVENTORY_COLUMNS;
            inventoryButtons[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(Constants.INVENTORY_BUTTON_SIZE.x * cellColumn, Constants.INVENTORY_BUTTON_SIZE.y * cellRow);
        }

        container.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, (int)(inventoryButtons.Count / Constants.INVENTORY_COLUMNS) * Constants.INVENTORY_BUTTON_SIZE.y);
    }

    public void onClickInventoryDescriptionClose()
    {
        setSelectedInventoryItem(null);
    }

    public void setSelectedInventoryItem(PlayerDataInventoryItem item)
    {
        if (item == null)
        {
            itemPopup.close();
        }
        else
        {
            itemPopup.setItem(item);
            itemPopup.open();
        }
    }

    public void setSelectedFurnitureItem(PlayerDataInventoryItem item)
    {
        if (item == null)
        {
            //panelInventoryItem.SetActive(false);
        }
        else
        {
            FurnitureController furniture = Constants.furniture[item.tag];
            if (currentScreen == SCREEN.WORKSHOP)
            {
                //panelInventoryItem.SetActive(true);
                //textInventoryItemName.text = furniture.resourceName;
                //textInventoryItemDescription.text = furniture.description;
                //textInventoryItemAmount.text = "";
                //imageInventoryItem.sprite = furniture.sprite;
            }
        }
    }
}
