﻿using UnityEngine;

public class GameScreen : Singleton<GameScreen>
{
    private float m_force;
    private float m_time;
    private bool m_shaking;
    private float m_val;

    private float m_forceReductionOverTime;
    private Vector3 m_originalPosition;

    private void Awake()
    {
        m_force = 0;
        m_shaking = false;
        m_forceReductionOverTime = 2f;
        m_originalPosition = transform.position;
    }

    public virtual void shake(float p_time, float p_force)
    {
        m_time = p_time;
        m_force = p_force;
        m_shaking = true;
        m_val = Mathf.PI / 2;

        GameCanvas.Instance.startShake();
    }

    public virtual void Update()
    {
        if (m_shaking)
        {
            transform.position = new Vector3(m_originalPosition.x + ((Mathf.Cos(m_val) * m_force) * Time.deltaTime),
                                             m_originalPosition.y + ((Mathf.Sin(m_val) * m_force) * Time.deltaTime),
                                             m_originalPosition.z);
            m_val += 90f * Time.deltaTime;
            m_force -= m_forceReductionOverTime * Time.deltaTime;
            m_time -= Time.deltaTime;

            if (m_time <= 0)
            {
                transform.position = m_originalPosition;
                m_shaking = false;
                GameCanvas.Instance.stopShake();
            }
        }
    }
}
