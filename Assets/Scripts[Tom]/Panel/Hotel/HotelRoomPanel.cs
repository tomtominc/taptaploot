﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotelRoomPanel : Panel
{
    public Image m_hotelRoomBackground;
    public List<Sprite> hotelRoomBackgrounds;

    public override void open()
    {
        int roomIndex = GameMainController.self.getCurrentHotelIndex();
        m_hotelRoomBackground.sprite = hotelRoomBackgrounds[roomIndex];

        gameObject.SetActive(true);
    }

    public override void close()
    {
        gameObject.SetActive(false);
    }
}
