﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DebugTool))]
class DebugToolEditor : Editor
{
    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Clear PlayerData"))
        {
            PlayerDataManager.clear();
            PlayerDataManager.save();
        }
    }
}