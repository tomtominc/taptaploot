﻿using UnityEngine;
using UnityEngine.UI;

public class FurnitureIngredientView : MonoBehaviour
{
    [SerializeField]
    private Image m_ingredientIcon;
    [SerializeField]
    private Text m_ingredientCountLabel;
    [SerializeField]
    private Color m_activeColor;
    [SerializeField]
    private Color m_disabledColor;

    public void initialize(RecipeIngredient p_ingredient)
    {
        m_ingredient = p_ingredient;
        m_material = Constants.materials[m_ingredient.resourceTag];

        updateView();
    }

    public void updateView()
    {
        PlayerData playerData = PlayerDataManager.getData();

        m_ingredientIcon.sprite = m_material.sprite;
        m_ingredientCountLabel.text = m_ingredient.amount.ToString();
        bool hasIngredient = playerData.inventoryItems.ContainsKey(m_ingredient.resourceTag) && playerData.inventoryItems[m_ingredient.resourceTag].count >= m_ingredient.amount;
        m_ingredientCountLabel.color = hasIngredient ? m_activeColor : m_disabledColor;

    }

    private MaterialController m_material;
    private RecipeIngredient m_ingredient;
}
