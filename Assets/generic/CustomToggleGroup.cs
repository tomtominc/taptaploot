﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomToggleGroup : MonoBehaviour
{
    private List<CustomToggle> toggles = new List<CustomToggle>();
    
    void Start ()
    {
    }
    
    void Update ()
    {
    }

    public void addToggle(CustomToggle toggle)
    {
        toggles.Add(toggle);
    }

    public List<CustomToggle> getToggles()
    {
        return toggles;
    }
}
