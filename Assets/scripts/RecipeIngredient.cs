﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RecipeIngredient
{
    public string resourceTag;
    public int amount;

    public RecipeIngredient()
    {
        this.resourceTag = "";
        this.amount = 0;
    }

    public RecipeIngredient(string resourceTag, int amount)
    {
        this.resourceTag = resourceTag;
        this.amount = amount;
    }
}
