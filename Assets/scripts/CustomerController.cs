﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerController : MonoBehaviour
{
    public enum STATE
    {
        IDLE, WALK, ADMIRE
    }

    public Vector2Int cellPosition;
    public Vector2Int targetPosition;

    private float idleTimer;
    private float walkTimer;
    private float stepTimer;
    private float admireTimer;
    private STATE state;
    private float idleDelay;
    private float walkDelay;
    private float stepDelay;
    private float admireDelay;
    private Vector2Int admireInDirection;
    private int admireSpot;
    
	void Start ()
    {
		setStateIdle();
	}
	
	void Update ()
    {
        Vector2 absoluteCellPosition = GameMainController.self.getCustomerAbsolutePosition(cellPosition);
        Vector2 absoluteTargetPosition = GameMainController.self.getCustomerAbsolutePosition(targetPosition);

        switch (state)
        {
        case STATE.IDLE:
            idleTimer += Time.deltaTime;
            if (idleTimer >= idleDelay)
            {
                setNewTarget(false);
                setStateWalk();
            }
            break;

        case STATE.WALK:
            transform.localPosition = Vector3.Lerp(absoluteCellPosition, absoluteTargetPosition, stepTimer / stepDelay);
		
            stepTimer += Time.deltaTime;
            walkTimer += Time.deltaTime;

            if (stepTimer >= stepDelay)
            {
                if (walkTimer >= walkDelay)
                {
                    if (scanForFurniture())
                    {
                        setStateAdmire();
                    }
                    else
                    {
                        setStateIdle();
                    }
                }
                else
                {
                    stepTimer -= stepDelay;
                    setNewTarget(true);
                }
            }
            break;

        case STATE.ADMIRE:
            admireTimer += Time.deltaTime;
            if (admireTimer >= admireDelay)
            {
                setNewTarget(false);
                setStateWalk();
            }
            break;
        }

        transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.y * 0.1f);
	}

    public void setStateAdmire()
    {
        state = STATE.ADMIRE;
        admireTimer = 0;
        admireDelay = Random.Range(Constants.CUSTOMER_ADMIRE_DELAY_MIN, Constants.CUSTOMER_ADMIRE_DELAY_MAX);
    }

    public void setStateIdle()
    {
        state = STATE.IDLE;
        idleTimer = 0;
        idleDelay = Random.Range(Constants.CUSTOMER_IDLE_DELAY_MIN, Constants.CUSTOMER_IDLE_DELAY_MAX);
    }

    public void setStateWalk()
    {
        state = STATE.WALK;
        walkTimer = 0;
        stepTimer = 0;
        stepDelay = Random.Range(Constants.CUSTOMER_STEP_DELAY_MIN, Constants.CUSTOMER_STEP_DELAY_MAX);
        walkDelay = Random.Range(Constants.CUSTOMER_WALK_DELAY_MIN, Constants.CUSTOMER_WALK_DELAY_MAX);
    }

    public void setNewTarget(bool prefersSameDirection)
    {
        if (prefersSameDirection)
        {
            Vector2Int preferredDirection = targetPosition + (targetPosition - cellPosition);
            cellPosition = targetPosition;
            targetPosition = GameMainController.self.getCustomerAdjacentPosition(targetPosition, preferredDirection);
        }
        else
        {
            cellPosition = targetPosition;
            targetPosition = GameMainController.self.getCustomerAdjacentPosition(targetPosition);
        }
    }

    public bool scanForFurniture()
    {
        Vector2Int directionRight = targetPosition + Vector2Int.right;
        Vector2Int directionDown = targetPosition + Vector2Int.down;
        Vector2Int directionLeft = targetPosition + Vector2Int.left;
        Vector2Int directionUp = targetPosition + Vector2Int.up;

        List<Vector2Int> furnitureInArea = new List<Vector2Int>();
        if (GameMainController.self.hotelRoomSpotWithinGrid(directionRight.x, directionRight.y) && !GameMainController.self.isHotelRoomSpotFreeAt(directionRight.x, directionRight.y)) furnitureInArea.Add(directionRight);
        if (GameMainController.self.hotelRoomSpotWithinGrid(directionDown.x, directionDown.y) && !GameMainController.self.isHotelRoomSpotFreeAt(directionDown.x, directionDown.y)) furnitureInArea.Add(directionDown);
        if (GameMainController.self.hotelRoomSpotWithinGrid(directionLeft.x, directionLeft.y) && !GameMainController.self.isHotelRoomSpotFreeAt(directionLeft.x, directionLeft.y)) furnitureInArea.Add(directionLeft);
        if (GameMainController.self.hotelRoomSpotWithinGrid(directionUp.x, directionUp.y) && !GameMainController.self.isHotelRoomSpotFreeAt(directionUp.x, directionUp.y)) furnitureInArea.Add(directionUp);
        
        if (furnitureInArea.Count > 0)
        {
            Vector2Int randomSpot = furnitureInArea[Random.Range(0, furnitureInArea.Count)];
            admireInDirection = targetPosition - randomSpot;
            admireSpot = GameMainController.self.getHotelRoomSpotIndexAt(randomSpot.x, randomSpot.y);
            return true;
        }

        return false;
    }
}
