﻿using UnityEngine;

public class GameCanvas : Singleton<GameCanvas>
{
    private Canvas m_canvas;

    private void Awake()
    {
        m_canvas = GetComponent<Canvas>();
    }

    public void startShake()
    {
        if (m_canvas.renderMode == RenderMode.ScreenSpaceCamera)
        {
            if (!Application.isEditor)
            {
                m_canvas.renderMode = RenderMode.WorldSpace;
            }
        }

    }

    public void stopShake()
    {
        if (m_canvas.renderMode == RenderMode.WorldSpace)
        {
            if (!Application.isEditor)
            {
                m_canvas.renderMode = RenderMode.ScreenSpaceCamera;
            }
        }
    }
}
