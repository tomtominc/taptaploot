﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataHotelRoom
{
    public bool locked;
    public bool cobwebsCleared;
    public PlayerDataHotelRoomSpot[] spots;
    public System.DateTime buildTime;
    public bool isBuilding;
}
