﻿using UnityEngine;
using UnityEngine.UI;

public class FurnitureRecipePopup : Panel
{
    [SerializeField]
    private RectTransform m_content;
    [SerializeField]
    private Text m_nameLabel;
    [SerializeField]
    private Text m_descriptionLabel;
    [SerializeField]
    private Image m_furnitureIcon;
    [SerializeField]
    private RectTransform m_ingredientLayout;
    [SerializeField]
    private Button m_buildButton;
    [SerializeField]
    private Text m_buildButtonLabel;
    [SerializeField]
    private Image m_buildButtonDivider;
    [SerializeField]
    private Color m_buildButtonLabelActiveColor;
    [SerializeField]
    private Color m_buildButtonLabelDisabledColor;

    public void initialize(FurnitureController p_furniture, FurnitureController p_upgrade, Vector2 p_position)
    {
        m_furniture = p_furniture;
        m_upgrade = p_upgrade;

        m_targetPosition = p_position;

        m_buildButton.onClick.AddListener(OnClickBuild);
    }

    public override void open()
    {
        gameObject.SetActive(true);

        //m_content.anchoredPosition = m_targetPosition;

        bool isMaxed = m_upgrade == null;

        if (isMaxed)
        {
            m_buildButtonLabel.color = m_buildButtonLabelDisabledColor;
            m_buildButtonDivider.color = m_buildButtonLabelDisabledColor;
            m_buildButtonLabel.text = "Maxed";
            m_buildButton.interactable = false;

            m_nameLabel.text = m_furniture.resourceName;
            m_descriptionLabel.text = m_furniture.description;
            m_furnitureIcon.sprite = m_furniture.sprite;

            for (int i = 0; i < m_ingredientLayout.childCount; i++)
            {
                m_ingredientLayout.GetChild(i).gameObject.SetActive(false);

            }
        }
        else
        {
            bool enoughIngredients = hasRecipeItems(m_upgrade.recipe);
            m_buildButton.interactable = enoughIngredients;
            m_buildButtonLabel.color = enoughIngredients ? m_buildButtonLabelActiveColor : m_buildButtonLabelDisabledColor;
            m_buildButtonLabel.text = enoughIngredients ? "Upgrade" : "Not enough materials";
            m_buildButtonDivider.color = enoughIngredients ? m_buildButtonLabelActiveColor : m_buildButtonLabelDisabledColor;

            m_nameLabel.text = m_upgrade.resourceName;
            m_descriptionLabel.text = m_upgrade.description;
            m_furnitureIcon.sprite = m_upgrade.sprite;

            for (int i = 0; i < m_ingredientLayout.childCount; i++)
            {
                Transform ingredientRect = m_ingredientLayout.GetChild(i);

                if (i < m_upgrade.recipe.Length)
                {
                    ingredientRect.gameObject.SetActive(true);

                    FurnitureIngredientView ingredientView = ingredientRect.GetComponent<FurnitureIngredientView>();
                    ingredientView.initialize(m_upgrade.recipe[i]);
                }
                else
                {
                    ingredientRect.gameObject.SetActive(false);
                }

            }
        }
    }

    public override void close()
    {
        m_buildButton.onClick.RemoveListener(OnClickBuild);
        gameObject.SetActive(false);
    }

    private void OnClickBuild()
    {
        UIMainController.self.onClickFurnitureBuild();
    }


    public bool hasRecipeItems(RecipeIngredient[] recipe)
    {
        bool canBuild = true;
        for (int i = 0; i < recipe.Length && canBuild; i++)
        {
            RecipeIngredient ingredient = recipe[i];
            if (!PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag)) canBuild = false;
            else canBuild &= PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count >= ingredient.amount;
        }
        return canBuild;
    }

    private FurnitureController m_furniture;
    private FurnitureController m_upgrade;
    private Vector2 m_targetPosition;
}
