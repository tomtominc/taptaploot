﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMainController : MonoBehaviour
{
    public static GameMainController self;

    public Light lightSun;
    public Transform spawnParent;
    public Transform furnitureParent;
    public CobwebController prefabCobweb;
    public CustomerController prefabCustomer;
    public SpawnDefinition[] gardenObjects;
    public SpawnDefinition[] creatures;
    public Image imageHotelBackground;

    private float spawnTimer;
    private MaterialController[] spawnGrid;
    private List<MaterialController> produceList;
    private List<AnimalController> creatureList;
    private List<HotelRoomSpotController> hotelRoomSpots;
    private List<CobwebController> cobwebs;
    private List<CustomerController> customers;
    private float creatureTimer;
    private int dayTimeSeconds;
    private bool gamePaused;
    private int currentGardenIndex;
    private int currentHotelIndex;
    private int cobwebsRemaining;
    private int[][] hotelRoomCollisionGrid;
    private int selectedHotelRoomIndex;
    private int selectedHotelRoomSpotIndex;

    void Awake()
    {
        self = this;
    }

    void Start()
    {
        gamePaused = true;

        creatureTimer = 0;

        currentGardenIndex = 0;

        updateDayTime();
        resumeGame();
    }

    void Update()
    {
        updateDayTime();

        spawnTimer += Time.deltaTime;
        if (spawnTimer >= Constants.SPAWN_DELAY)
        {
            spawnTimer = 0;
            spawnRandomGardenObject(true);
        }

        if (canSpawnCreature())
        {
            creatureTimer += Time.deltaTime;
            if (creatureTimer >= Constants.CREATURE_DELAY)
            {
                creatureTimer = 0;
                spawnRandomCreature(new Vector2(getOffscreenSize() * (Random.Range(0, 2) == 0 ? -1 : 1), Random.Range(-Constants.SPAWN_AREA_SIZE.y, Constants.SPAWN_AREA_SIZE.y) / 2));
            }
        }
        else
        {
            creatureTimer = 0;
        }

        updateSun();
        updateProduceList();
        updateCreatureList();
    }

    public FurnitureController getCurrentFurniture(int roomIndex, int spotIndex)
    {
        HotelRoomController room = Constants.hotelRooms[roomIndex];
        HotelRoomSpotController spot = Constants.hotelRoomSpots[room.spots[spotIndex]];
        int currentLevel = PlayerDataManager.getData().hotelrooms[roomIndex].spots[spotIndex].level;
        if (spot.furniture.Length > currentLevel)
        {
            return Constants.furniture[spot.furniture[currentLevel]];
        }
        return null;
    }

    public FurnitureController getNextFurnitureUpgrade(int roomIndex, int spotIndex)
    {
        HotelRoomController room = Constants.hotelRooms[roomIndex];
        HotelRoomSpotController spot = Constants.hotelRoomSpots[room.spots[spotIndex]];
        int currentLevel = PlayerDataManager.getData().hotelrooms[roomIndex].spots[spotIndex].level;
        if (spot.furniture.Length > currentLevel + 1)
        {
            return Constants.furniture[spot.furniture[currentLevel + 1]];
        }
        return null;
    }

    public FurnitureController getNextSelectedFurnitureUpgrade()
    {
        return getNextFurnitureUpgrade(selectedHotelRoomIndex, selectedHotelRoomSpotIndex);
    }

    public FurnitureController getCurrentSelectedFurniture()
    {
        return getCurrentFurniture(selectedHotelRoomIndex, selectedHotelRoomSpotIndex);
    }

    public void buildSelectedHotelRoom()
    {
        HotelRoomController hotelRoomToBuild = getSelectedHotelRoomController();
        consumeRecipeItems(hotelRoomToBuild.recipe);
        consumeCurrency(hotelRoomToBuild.currencyRequired);
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].isBuilding = true;
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].buildTime = System.DateTime.Now;
    }

    public void unlockSelectedHotelRoom()
    {
        HotelRoomController hotelRoomToUnlock = getSelectedHotelRoomController();
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].isBuilding = false;
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].locked = false;
        setHotelRoom(selectedHotelRoomIndex);
    }

    public float getHotelRoomBuildProgress(int hotelRoomIndex)
    {
        System.TimeSpan buildSpan = new System.TimeSpan(PlayerDataManager.getData().hotelrooms[hotelRoomIndex].buildTime.Ticks - System.DateTime.Now.Ticks);
        int maxTimeInSeconds = Constants.getHotelRoomBuildTime(hotelRoomIndex);
        if (buildSpan.TotalSeconds >= maxTimeInSeconds) return 1f;
        else if (buildSpan.TotalMilliseconds <= 0) return 0f;
        else return (float)(buildSpan.TotalSeconds / maxTimeInSeconds);
    }

    public float getSelectedHotelRoomBuildProgress()
    {
        return getHotelRoomBuildProgress(selectedHotelRoomIndex);
    }

    public void buildNextSelectedFurniture()
    {
        FurnitureController furnitureToUnlock = getNextSelectedFurnitureUpgrade();
        consumeRecipeItems(furnitureToUnlock.recipe);
        consumeCurrency(furnitureToUnlock.currencyRequired);
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].isBuilding = true;
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].buildTime = System.DateTime.Now;
    }
    
    public float getSelectedFurnitureBuildProgress()
    {
        System.TimeSpan buildSpan = new System.TimeSpan(PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].buildTime.Ticks - System.DateTime.Now.Ticks);
        int maxTimeInSeconds = Constants.getFurnitureBuildTime(selectedHotelRoomIndex, selectedHotelRoomSpotIndex, PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].level);
        if (buildSpan.TotalSeconds >= maxTimeInSeconds) return 1f;
        else if (buildSpan.TotalMilliseconds <= 0) return 0f;
        else return (float)(buildSpan.TotalSeconds / maxTimeInSeconds);
    }

    public void unlockNextSelectedFurniture()
    {
        FurnitureController furnitureToUnlock = getNextSelectedFurnitureUpgrade();
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].isBuilding = false;
        PlayerDataManager.getData().hotelrooms[selectedHotelRoomIndex].spots[selectedHotelRoomSpotIndex].level++;
        PlayerDataManager.getData().hotelExperience += furnitureToUnlock.experience;
        if (PlayerDataManager.getData().hotelExperience >= Constants.getHotelExperience(PlayerDataManager.getData().hotelLevel))
        {
            PlayerDataManager.getData().hotelExperience -= Constants.getHotelExperience(PlayerDataManager.getData().hotelLevel);
            PlayerDataManager.getData().hotelLevel++;
        }
        updateHotelRoomSpot(selectedHotelRoomSpotIndex);
    }

    public void setSelectedHotelRoomIndex(int i)
    {
        selectedHotelRoomIndex = i;
    }

    public void setSelectedHotelRoomSpotIndex(int i)
    {
        selectedHotelRoomSpotIndex = i;
    }

    public HotelRoomController getSelectedHotelRoomController()
    {
        return Constants.hotelRooms[selectedHotelRoomIndex];
    }

    public bool hasCurrency(int c)
    {
        return PlayerDataManager.getData().currency >= c;
    }

    public void consumeCurrency(int c)
    {
        PlayerDataManager.getData().currency -= c;
    }

    public bool hasSelectedHotelRoomRecipeItems()
    {
        HotelRoomController h = getSelectedHotelRoomController();
        return hasRecipeItems(h.recipe);
    }

    public bool hasNextSelectedFurnitureRecipeItems()
    {
        FurnitureController f = getNextSelectedFurnitureUpgrade();
        return hasRecipeItems(f.recipe);
    }

    public bool hasSelectedHotelRoomCurrency()
    {
        HotelRoomController h = getSelectedHotelRoomController();
        return hasCurrency(h.currencyRequired);
    }

    public bool hasNextSelectedFurnitureCurrency()
    {
        FurnitureController f = getNextSelectedFurnitureUpgrade();
        return hasCurrency(f.currencyRequired);
    }

    public bool hasRecipeItems(RecipeIngredient[] recipe)
    {
        if (recipe == null) return false;
        bool canBuild = true;
        for (int i = 0; i < recipe.Length && canBuild; i++)
        {
            RecipeIngredient ingredient = recipe[i];
            if (!PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag)) canBuild = false;
            else if (PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count < ingredient.amount) canBuild = false;
        }
        return canBuild;
    }

    public void consumeRecipeItems(RecipeIngredient[] recipe)
    {
        for (int i = 0; i < recipe.Length; i++)
        {
            RecipeIngredient ingredient = recipe[i];
            if (PlayerDataManager.getData().inventoryItems.ContainsKey(ingredient.resourceTag))
            {
                PlayerDataManager.getData().inventoryItems[ingredient.resourceTag].count -= ingredient.amount;
            }
        }
    }

    public void updateDayTime()
    {
        System.DateTime dayTime = System.DateTime.Now;
        dayTimeSeconds = dayTime.Hour * Constants.SECONDS_IN_HOUR + dayTime.Minute * Constants.SECONDS_IN_MINUTE + dayTime.Second;
    }

    public void updateSun()
    {
        int dayTimeHour = (int)(dayTimeSeconds / Constants.SECONDS_IN_HOUR);
        float hourDelta = (float)(dayTimeSeconds % Constants.SECONDS_IN_HOUR) / (float)Constants.SECONDS_IN_HOUR;

        if (dayTimeHour == 5)
        {
            lightSun.color = Color.Lerp(Constants.COLOR_NIGHT, Constants.COLOR_SUNSET, hourDelta);
        }
        else if (dayTimeHour == 6)
        {
            lightSun.color = Color.Lerp(Constants.COLOR_SUNSET, Constants.COLOR_DAY, hourDelta);
        }
        else if (dayTimeHour > 6 && dayTimeHour < 17)
        {
            lightSun.color = Constants.COLOR_DAY;
        }
        else if (dayTimeHour == 17)
        {
            lightSun.color = Color.Lerp(Constants.COLOR_DAY, Constants.COLOR_SUNSET, hourDelta);
        }
        else if (dayTimeHour == 18)
        {
            lightSun.color = Color.Lerp(Constants.COLOR_SUNSET, Constants.COLOR_NIGHT, hourDelta);
        }
        else
        {
            lightSun.color = Constants.COLOR_NIGHT;
        }
    }

    public float getDayTimeDelta()
    {
        return dayTimeSeconds / (float)Constants.SECONDS_IN_DAY;
    }

    public void addMaterial(string tag, int count)
    {
        if (!Constants.materials.ContainsKey(tag)) return;
        MaterialController material = Constants.materials[tag];
        switch (material.type)
        {
            case MaterialController.TYPE.CURRENCY: PlayerDataManager.getData().currency += material.amount * count; break;
            case MaterialController.TYPE.MATERIAL:
                if (!PlayerDataManager.getData().inventoryItems.ContainsKey(tag))
                {
                    PlayerDataManager.getData().inventoryItems.Add(tag, new PlayerDataInventoryItem(PlayerDataInventoryItem.TYPE.MATERIAL, tag, 0));
                }
                PlayerDataManager.getData().inventoryItems[tag].count += material.amount * count;
                break;
        }
    }

    public void addGardenExperience(int experience)
    {
        PlayerDataGarden garden = getCurrentGarden();
        int experienceNeeded = Constants.getGardenExperience(garden.level);

        garden.experience += experience;
        if (garden.experience >= experienceNeeded)
        {
            garden.level++;
            garden.experience -= experienceNeeded;

            if (currentGardenIndex + 1 < PlayerDataManager.getData().gardens.Count && garden.level >= Constants.GARDEN_UNLOCK_LEVELS[currentGardenIndex + 1] && PlayerDataManager.getData().gardens[currentGardenIndex + 1].locked)
            {
                unlockGarden(currentGardenIndex + 1);
                UIMainController.self.onUnlockedGarden(currentGardenIndex + 1);
            }

            UIMainController.self.onGardenLevelUp(currentGardenIndex);
        }
    }

    public void unlockGarden(int i)
    {
        PlayerDataGarden unlockedGarden = PlayerDataManager.getData().gardens[i];
        unlockedGarden.locked = false;
        unlockedGarden.lastPlayTime = System.DateTime.Now;
        unlockedGarden.objectsToSpawn = Constants.GARDEN_START_OBJECTS;
    }

    public void addFurniture(string tag)
    {
        if (!Constants.furniture.ContainsKey(tag)) return;
        FurnitureController furniture = Constants.furniture[tag];
        if (!PlayerDataManager.getData().inventoryItems.ContainsKey(tag))
        {
            PlayerDataManager.getData().inventoryItems.Add(tag, new PlayerDataInventoryItem(PlayerDataInventoryItem.TYPE.FURNITURE, tag, 0));
        }
        PlayerDataManager.getData().inventoryItems[tag].count++;
    }

    public float getSpawnTimeRemaining()
    {
        return Constants.SPAWN_DELAY - spawnTimer;
    }

    public int getCurrency()
    {
        return PlayerDataManager.getData().currency;
    }

    public void spawnRandomGardenObject(bool animateAppearance)
    {
        List<int> freeCells = getFreeCells();

        if (freeCells.Count > 0)
        {
            int randomCell = freeCells[Random.Range(0, freeCells.Count)];

            Vector2 randomMargin = new Vector2(Random.Range(-Constants.SPAWN_RANDOM_MARGIN.x, Constants.SPAWN_RANDOM_MARGIN.x), Random.Range(-Constants.SPAWN_RANDOM_MARGIN.y, Constants.SPAWN_RANDOM_MARGIN.y));
            SpawnDefinition objectDefinition = SpawnDefinition.getRandomObject(gardenObjects);
            spawnObject(objectDefinition.tag, randomCell, randomMargin, true, animateAppearance);
        }
    }

    public MaterialController spawnObject(string tag, int cell, Vector2 position, bool alignToCell, bool animateAppearance)
    {
        MaterialController obj = Constants.getMaterialController(tag);
        if (obj == null) return null;
        Vector2 spawnPosition = position;
        if (alignToCell) spawnPosition += new Vector2((cell % Constants.SPAWN_COLUMNS) * Constants.SPAWN_SIZE.x + Constants.SPAWN_SIZE.x / 2 - Constants.SPAWN_AREA_SIZE.x / 2, (cell / Constants.SPAWN_COLUMNS) * Constants.SPAWN_SIZE.y + Constants.SPAWN_SIZE.y / 2 - Constants.SPAWN_AREA_SIZE.y / 2);
        MaterialController newLoot = obj.spawn(spawnParent, spawnPosition.x, spawnPosition.y, animateAppearance);
        newLoot.setGridIndex(cell);
        spawnGrid[cell] = newLoot;
        return newLoot;
    }

    public List<int> getFreeCells()
    {
        List<int> freeCells = new List<int>();
        for (int i = 0; i < spawnGrid.Length; i++)
        {
            if (spawnGrid[i] == null) freeCells.Add(i);
        }
        return freeCells;
    }

    public MaterialController getGridObject(int i)
    {
        return spawnGrid[i];
    }

    public void setGridObject(MaterialController obj, int i)
    {
        spawnGrid[i] = obj;
    }

    public bool canAddProduce()
    {
        return produceList.Count < Constants.PRODUCE_MAX_COUNT;
    }

    public MaterialController spawnProduce(string tag, Vector2 position, bool animateAppearance)
    {
        MaterialController obj = Constants.getMaterialController(tag);
        if (obj == null) return null;
        if (!canAddProduce()) return null;
        MaterialController newProduce = obj.spawn(spawnParent, position.x, position.y, animateAppearance);
        produceList.Add(newProduce);
        return newProduce;
    }

    public void updateProduceList()
    {
        for (int i = produceList.Count - 1; i >= 0; i--)
        {
            if (produceList[i] == null) produceList.RemoveAt(i);
        }
    }

    public bool canSpawnCreature()
    {
        return creatureList.Count < Constants.CREATURE_MAX_COUNT;
    }

    public PlayerDataGarden getCurrentGarden()
    {
        return PlayerDataManager.getData().gardens[currentGardenIndex];
    }

    public PlayerDataHotelRoom getCurrentHotelRoom()
    {
        return PlayerDataManager.getData().hotelrooms[currentHotelIndex];
    }

    public int getCurrentGardenIndex()
    {
        return currentGardenIndex;
    }

    public int getCurrentHotelIndex()
    {
        return currentHotelIndex;
    }

    public AnimalController spawnRandomCreature(Vector2 position)
    {
        if (!canSpawnCreature()) return null;
        SpawnDefinition randomObject = SpawnDefinition.getRandomObject(creatures);
        if (!Constants.animals.ContainsKey(randomObject.tag)) return null;
        AnimalController newCreature = Instantiate<GameObject>(Constants.animals[randomObject.tag].gameObject).GetComponent<AnimalController>();
        newCreature.transform.SetParent(spawnParent);
        newCreature.transform.localPosition = position;
        creatureList.Add(newCreature);
        return newCreature;
    }

    public void updateCreatureList()
    {
        for (int i = creatureList.Count - 1; i >= 0; i--)
        {
            if (creatureList[i] == null) creatureList.RemoveAt(i);
        }
    }

    public float getOffscreenSize()
    {
        return UIMainController.self.cameraMain.orthographicSize * UIMainController.self.cameraMain.aspect + Constants.OFFSCREEN_MARGIN;
    }

    public void setGarden(int i)
    {
        if (currentGardenIndex != i)
        {
            saveCurrentGarden();
            currentGardenIndex = i;
            loadCurrentGarden();
        }
    }

    public void setHotelRoom(int i)
    {
        if (currentHotelIndex != i)
        {
            saveCurrentHotel();
            currentHotelIndex = i;
            loadCurrentHotel();
        }
    }

    public void resetHotelRoom()
    {
        int i;

        if (hotelRoomSpots != null)
        {
            for (i = 0; i < hotelRoomSpots.Count; i++)
            {
                if (hotelRoomSpots[i] != null && hotelRoomSpots[i].gameObject != null) Destroy(hotelRoomSpots[i].gameObject);
            }
        }

        hotelRoomSpots = new List<HotelRoomSpotController>();

        if (cobwebs != null)
        {
            for (i = 0; i < cobwebs.Count; i++)
            {
                if (cobwebs[i] != null && cobwebs[i].gameObject != null) Destroy(cobwebs[i].gameObject);
            }
        }

        cobwebs = new List<CobwebController>();

        if (customers != null)
        {
            for (i = 0; i < customers.Count; i++)
            {
                if (customers[i] != null && customers[i].gameObject != null) Destroy(customers[i].gameObject);
            }
        }

        customers = new List<CustomerController>();
    }

    public void updateHotelRoomSpot(int i)
    {
        hotelRoomSpots[i].setLevel(PlayerDataManager.getData().hotelrooms[getCurrentHotelIndex()].spots[i].level);
    }

    public void resetGarden()
    {
        int i;

        if (spawnGrid != null)
        {
            for (i = 0; i < spawnGrid.Length; i++)
            {
                if (spawnGrid[i] != null && spawnGrid[i].gameObject != null) Destroy(spawnGrid[i].gameObject);
            }
        }

        spawnGrid = new MaterialController[Constants.SPAWN_CELLS];
        for (i = 0; i < spawnGrid.Length; i++)
        {
            spawnGrid[i] = null;
        }

        if (produceList != null)
        {
            for (i = 0; i < produceList.Count; i++)
            {
                if (produceList[i] != null && produceList[i].gameObject != null) Destroy(produceList[i].gameObject);
            }
        }

        produceList = new List<MaterialController>();

        if (creatureList != null)
        {
            for (i = 0; i < creatureList.Count; i++)
            {
                if (creatureList[i] != null && creatureList[i].gameObject != null) Destroy(creatureList[i].gameObject);
            }
        }

        creatureList = new List<AnimalController>();
    }

    public int getGardenContentCount(int g)
    {
        if (getCurrentGardenIndex() == g)
        {
            int materialCount = 0;
            foreach (MaterialController material in spawnGrid)
            {
                if (material != null) materialCount++;
            }
            return materialCount;
        }
        else
        {
            if (PlayerDataManager.getData().gardens.Count > g)
            {
                PlayerDataGarden garden = PlayerDataManager.getData().gardens[g];
                if (!garden.locked && garden.gardenItems != null)
                {
                    int objectsToSpawn = 0;

                    if (garden.objectsToSpawn > 0)
                    {
                        objectsToSpawn = garden.objectsToSpawn;
                    }
                    else
                    {
                        System.TimeSpan timeDiff = new System.TimeSpan(System.DateTime.Now.Ticks - garden.lastPlayTime.Ticks);
                        if (timeDiff.CompareTo(System.TimeSpan.Zero) >= 0)
                        {
                            objectsToSpawn = Mathf.Min(Constants.SPAWN_CELLS, (int)(timeDiff.TotalSeconds / Constants.SPAWN_DELAY_IDLE));
                        }
                    }
                    return Mathf.Min(garden.gardenItems.Count + objectsToSpawn, Constants.SPAWN_CELLS);
                }
            }
        }
        return 0;
    }

    public float getGardenFilledProgress(int g)
    {
        return (float)getGardenContentCount(g) / (float)Constants.SPAWN_CELLS;
    }

    public void saveCurrentGarden()
    {
        int i;

        PlayerDataGarden garden = getCurrentGarden();
        garden.lastPlayTime = System.DateTime.Now;
        garden.gardenItems = new List<PlayerDataGardenItem>();
        for (i = 0; i < spawnGrid.Length; i++)
        {
            if (spawnGrid[i] != null)
            {
                garden.gardenItems.Add(new PlayerDataGardenItem(spawnGrid[i].resourceTag, i, spawnGrid[i].transform.localPosition.x, spawnGrid[i].transform.localPosition.y));
            }
        }

        garden.produceItems = new List<PlayerDataProduceItem>();
        for (i = 0; i < produceList.Count; i++)
        {
            garden.produceItems.Add(new PlayerDataProduceItem(produceList[i].resourceTag, produceList[i].transform.localPosition.x, produceList[i].transform.localPosition.y));
        }
    }

    public void loadCurrentGarden()
    {
        int i;

        resetGarden();

        PlayerDataGarden garden = getCurrentGarden();
        if (garden.gardenItems != null)
        {
            foreach (PlayerDataGardenItem g in garden.gardenItems)
            {
                spawnObject(g.tag, g.cell, new Vector2(g.x, g.y), false, false);
            }
        }

        if (garden.produceItems != null)
        {
            foreach (PlayerDataProduceItem p in garden.produceItems)
            {
                spawnProduce(p.tag, new Vector2(p.x, p.y), false);
            }
        }

        int objectsToSpawn = 0;
        if (garden.objectsToSpawn > 0)
        {
            objectsToSpawn = garden.objectsToSpawn;
            garden.objectsToSpawn = 0;
        }
        else
        {
            System.TimeSpan timeDiff = new System.TimeSpan(System.DateTime.Now.Ticks - garden.lastPlayTime.Ticks);
            if (timeDiff.CompareTo(System.TimeSpan.Zero) >= 0)
            {
                objectsToSpawn = Mathf.Min(Constants.SPAWN_CELLS, (int)(timeDiff.TotalSeconds / Constants.SPAWN_DELAY_IDLE));
            }
        }

        for (i = 0; i < objectsToSpawn; i++)
        {
            spawnRandomGardenObject(false);
        }
    }

    public void saveCurrentHotel()
    {

    }

    public void loadCurrentHotel()
    {
        resetHotelRoom();

        HotelRoomController hotelRoom = Constants.hotelRooms[getCurrentHotelIndex()];
        PlayerDataHotelRoom playerDataHotelRoom = PlayerDataManager.getData().hotelrooms[getCurrentHotelIndex()];
        if (playerDataHotelRoom.cobwebsCleared)
        {
            for (int i = 0; i < hotelRoom.spots.Length; i++)
            {
                string spotTag = hotelRoom.spots[i];
                if (Constants.hotelRoomSpots.ContainsKey(spotTag))
                {
                    HotelRoomSpotController newSpot = Instantiate<HotelRoomSpotController>(Constants.hotelRoomSpots[spotTag]);
                    newSpot.transform.SetParent(furnitureParent);
                    newSpot.transform.localPosition = newSpot.roomPosition;
                    newSpot.GetComponent<BoxCollider2D>().size = newSpot.colliderSize;
                    newSpot.GetComponent<BoxCollider2D>().offset = newSpot.colliderOffset;
                    newSpot.setLevel(PlayerDataManager.getData().hotelrooms[getCurrentHotelIndex()].spots[i].level);
                    hotelRoomSpots.Add(newSpot);
                }
                else
                {
                    Debug.LogWarningFormat("Hotel does not contain spotTag {0}", spotTag);
                }
            }

            int gridColumns = (int)(hotelRoom.maskSize.x / Constants.HOTEL_ROOM_GRID_SIZE.x);
            int gridRows = (int)(hotelRoom.maskSize.y / Constants.HOTEL_ROOM_GRID_SIZE.y);

            hotelRoomCollisionGrid = new int[gridColumns][];

            for (int x = 0; x < gridColumns; x++)
            {
                hotelRoomCollisionGrid[x] = new int[gridRows];
                for (int y = 0; y < gridRows; y++)
                {
                    Vector2 cell = getCustomerAbsolutePosition(new Vector2Int(x, y));
                    int collidesWith = -1;
                    foreach (HotelRoomSpotController spot in hotelRoomSpots)
                    {
                        Rect spotRect = new Rect(spot.transform.localPosition.x - spot.colliderSize.x / 2, spot.transform.localPosition.y - spot.colliderSize.y / 2, spot.colliderSize.x, spot.colliderSize.y);
                        if (spotRect.Contains(cell))
                        {
                            collidesWith = spot.index;
                        }
                    }
                    hotelRoomCollisionGrid[x][y] = collidesWith;
                }
            }

            spawnRandomCustomer();
            spawnRandomCustomer();
        }
        else
        {
            imageHotelBackground.material.SetFloat("_Saturation", -1);
            cobwebsRemaining = Constants.NUM_COBWEBS;
            for (int i = 0; i < Constants.NUM_COBWEBS; i++)
            {
                CobwebController newCobweb = Instantiate<CobwebController>(prefabCobweb);
                newCobweb.transform.SetParent(furnitureParent);
                newCobweb.transform.localPosition = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0) * Constants.COBWEB_SPAWN_RADIUS;
                newCobweb.transform.localScale = Vector3.one * Random.Range(Constants.COBWEB_MIN_SCALE, Constants.COBWEB_MAX_SCALE);
                cobwebs.Add(newCobweb);
            }
        }
    }

    public int getHotelRoomSpotIndexAt(int x, int y)
    {
        return hotelRoomCollisionGrid[x][y];
    }

    public bool isHotelRoomSpotFreeAt(int x, int y)
    {
        return hotelRoomCollisionGrid[x][y] == -1;
    }

    public bool hotelRoomSpotWithinGrid(int x, int y)
    {
        return x >= 0 && y >= 0 && hotelRoomCollisionGrid.Length > x && hotelRoomCollisionGrid[x].Length > y;
    }

    public void spawnRandomCustomer()
    {
        HotelRoomController hotelRoom = Constants.hotelRooms[getCurrentHotelIndex()];

        List<Vector2Int> freeCells = new List<Vector2Int>();
        for (int x = 0; x < hotelRoomCollisionGrid.Length; x++)
        {
            for (int y = 0; y < hotelRoomCollisionGrid[x].Length; y++)
            {
                if (isHotelRoomSpotFreeAt(x, y) && !customerAtPosition(new Vector2Int(x, y))) freeCells.Add(new Vector2Int(x, y));
            }
        }

        Vector2Int randomCell = freeCells[Random.Range(0, freeCells.Count)];
        spawnCustomer(randomCell);
    }

    public void spawnCustomer(Vector2Int p)
    {
        CustomerController newCustomer = Instantiate<CustomerController>(prefabCustomer);
        newCustomer.targetPosition = p;
        newCustomer.cellPosition = p;
        newCustomer.transform.SetParent(furnitureParent);
        newCustomer.transform.localPosition = getCustomerAbsolutePosition(p);
        newCustomer.transform.localScale = Vector3.one;
        customers.Add(newCustomer);
    }

    public Vector2 getCustomerAbsolutePosition(Vector2Int p)
    {
        HotelRoomController hotelRoom = Constants.hotelRooms[getCurrentHotelIndex()];
        return new Vector2((p.x + 0.5f) * Constants.HOTEL_ROOM_GRID_SIZE.x + hotelRoom.maskOffset.x - hotelRoom.maskSize.x / 2, (p.y + 0.5f) * Constants.HOTEL_ROOM_GRID_SIZE.y + hotelRoom.maskOffset.y - hotelRoom.maskSize.y / 2);
    }

    public Vector2Int getCustomerAdjacentPosition(Vector2Int p)
    {
        return getCustomerAdjacentPosition(p, p);
    }

    public Vector2Int getCustomerAdjacentPosition(Vector2Int p, Vector2Int preferredPosition)
    {
        if (!p.Equals(preferredPosition) && !customerAtPosition(preferredPosition) && preferredPosition.x >= 0 && preferredPosition.y >= 0 && preferredPosition.x < hotelRoomCollisionGrid.Length && preferredPosition.y < hotelRoomCollisionGrid[preferredPosition.x].Length && isHotelRoomSpotFreeAt(preferredPosition.x, preferredPosition.y)) return preferredPosition;

        List<Vector2Int> possiblePositions = new List<Vector2Int>();
        if (p.x > 0 && isHotelRoomSpotFreeAt(p.x - 1, p.y) && !customerAtPosition(new Vector2Int(p.x - 1, p.y))) possiblePositions.Add(new Vector2Int(p.x - 1, p.y));
        if (p.y > 0 && isHotelRoomSpotFreeAt(p.x, p.y - 1) && !customerAtPosition(new Vector2Int(p.x, p.y - 1))) possiblePositions.Add(new Vector2Int(p.x, p.y - 1));
        if (p.x + 1 < hotelRoomCollisionGrid.Length && isHotelRoomSpotFreeAt(p.x + 1, p.y) && !customerAtPosition(new Vector2Int(p.x + 1, p.y))) possiblePositions.Add(new Vector2Int(p.x + 1, p.y));
        if (p.y + 1 < hotelRoomCollisionGrid[p.x].Length && isHotelRoomSpotFreeAt(p.x, p.y + 1) && !customerAtPosition(new Vector2Int(p.x, p.y + 1))) possiblePositions.Add(new Vector2Int(p.x, p.y + 1));

        if (possiblePositions.Count == 0) return p;
        else return possiblePositions[Random.Range(0, possiblePositions.Count)];
    }

    public bool customerAtPosition(Vector2Int p)
    {
        foreach (CustomerController customer in customers)
        {
            if (customer.targetPosition.Equals(p) || customer.cellPosition.Equals(p)) return true;
        }
        return false;
    }

    public void removeCobweb(CobwebController cobweb)
    {
        Destroy(cobweb.gameObject);
        cobwebsRemaining--;
        imageHotelBackground.material.SetFloat("_Saturation", -((float)cobwebsRemaining / (float)Constants.NUM_COBWEBS));
        if (cobwebsRemaining <= 0)
        {
            PlayerDataManager.getData().hotelrooms[getCurrentHotelIndex()].cobwebsCleared = true;
            loadCurrentHotel();
        }
    }

    public void suspendGame()
    {
        if (!gamePaused)
        {
            PlayerDataManager.getData().lastGarden = currentGardenIndex;
            PlayerDataManager.getData().lastHotelRoom = currentHotelIndex;
            saveCurrentGarden();
            saveCurrentHotel();
            PlayerDataManager.save();
        }
        gamePaused = true;

    }

    public void resumeGame()
    {
        if (gamePaused)
        {
            PlayerDataManager.load();
            currentGardenIndex = PlayerDataManager.getData().lastGarden;
            currentHotelIndex = PlayerDataManager.getData().lastHotelRoom;
            loadCurrentGarden();
            loadCurrentHotel();
        }
        gamePaused = false;
    }

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus) suspendGame();
        else resumeGame();
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (!hasFocus) suspendGame();
        else resumeGame();
    }

    void OnApplicationQuit()
    {
        suspendGame();
    }
}
