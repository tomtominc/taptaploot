﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GardenSelectionPanel : Panel
{
    public const float ANIMATION_DURATION = 1F;
    public const float EASE_OVERSHOOT = 1.2F;

    [SerializeField]
    private ScrollRect m_selectionRect;
    [SerializeField]
    private RectTransform m_gardenSelectButtonPrefab;
    [SerializeField]
    private CanvasGroup m_background;

    public override void open()
    {
        gameObject.SetActive(true);

        RectTransform content = m_selectionRect.content;

        for (int i = content.childCount - 1; i > -1; i--)
        {
            Destroy(content.GetChild(i).gameObject);
        }

        PlayerData playerData = PlayerDataManager.getData();
        int numGardens = Constants.GARDEN_NAMES.Length;

        for (int i = 0; i < numGardens; i++)
        {
            PlayerDataGarden garden = playerData.gardens[i];
            RectTransform gardenSelectButtonRect = Instantiate(m_gardenSelectButtonPrefab);
            gardenSelectButtonRect.SetParent(content, false);
            GardenSelectButton gardenButton = gardenSelectButtonRect.GetComponent<GardenSelectButton>();
            gardenButton.initialize(Constants.GARDEN_NAMES[i], i, garden);
            gardenButton.onClick += OnGardenClicked;
        }

        if (numGardens > 0)
        {
            RectTransform gardenLabelRect = content.GetChild(0) as RectTransform;
            float gardenLabelPreferredAspect = GardenSelectButton.PREFERRED_SIZE.y / GardenSelectButton.PREFERRED_SIZE.x;
            float width = gardenLabelRect.sizeDelta.x;
            float height = width * gardenLabelPreferredAspect;
            float totalHeight = height * numGardens;
            content.sizeDelta = new Vector2(content.sizeDelta.x, totalHeight);
        }

        if (m_outTween != null && m_outTween.IsActive())
        {
            m_outTween.Kill();
        }

        m_background.alpha = 0;
        m_background.DOFade(1, 1);

        RectTransform viewPort = m_selectionRect.transform as RectTransform;

        viewPort.anchoredPosition = new Vector2(0f, -viewPort.sizeDelta.y);
        m_inTween = viewPort.DOAnchorPos(new Vector2(0f, -32f), ANIMATION_DURATION)
                .SetEase(Ease.OutBack, EASE_OVERSHOOT);
    }

    private void OnGardenClicked(int p_index)
    {
        UIMainController.self.onClickMapGarden(p_index);
    }

    public override void close()
    {
        if (gameObject.activeSelf)
        {
            if (m_inTween != null && m_inTween.IsActive())
            {
                m_inTween.Kill();
            }

            m_background.alpha = 1;
            m_background.DOFade(0, 1);

            RectTransform viewPort = m_selectionRect.transform as RectTransform;

            m_outTween = viewPort.DOAnchorPos(new Vector2(0f, -viewPort.sizeDelta.y), ANIMATION_DURATION)
                    .SetEase(Ease.InBack, EASE_OVERSHOOT)
                    .OnComplete(() => gameObject.SetActive(false));
        }
    }

    private Tweener m_inTween;
    private Tweener m_outTween;
}
