﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    public Animator animator;
    public SpriteRenderer spriteRenderer;
    
    void Start ()
    {
        
    }
    
    void Update ()
    {
        
    }

    public void setAnimation(string animation)
    {
        animator.Play(animation);
    }

    public void setAnimator(RuntimeAnimatorController animatorController)
    {
        animator.runtimeAnimatorController = animatorController;
    }
}
