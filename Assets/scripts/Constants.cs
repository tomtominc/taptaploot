﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Constants : MonoBehaviour
{
    public const string PREFAB_PATH = "Prefabs";
    public static float ASPECT_MIN = 1f / 2f;
    public static float ASPECT_MAX = 1f / 1f;
    public static float CAMERA_MIN = 5.4f;
    public static float CAMERA_MAX = 10.8f;
    public static float CAMERA_Z = -10;
    public static int INVENTORY_COLUMNS = 4;
    public static Vector3 INVENTORY_BUTTON_SIZE = new Vector3(225, 225, 0);
    public static float LEVEL_METER_MIN_SIZE = 96;
    public static float LEVEL_METER_MAX_SIZE = 720;

    public static float SPAWN_DELAY = 5f;
    public static float SPAWN_DELAY_IDLE = 10f;
    public static float CREATURE_DELAY = 5f;
    public static float NEW_GARDEN_CONTENT_DENSITY = 0.25f;
    public static Vector2 SPAWN_AREA_SIZE = new Vector2(10, 10);
    public static Vector2 SPAWN_SIZE = new Vector2(1.5f, 1.5f);
    public static Vector2 SPAWN_RANDOM_MARGIN = new Vector2(0.25f, 0.25f);
    public static int PRODUCE_MAX_COUNT = 20;
    public static int CREATURE_MAX_COUNT = 3;
    public static float OFFSCREEN_MARGIN = 2f;
    public static int SPAWN_ROWS = (int)(SPAWN_AREA_SIZE.y / SPAWN_SIZE.y);
    public static int SPAWN_COLUMNS = (int)(SPAWN_AREA_SIZE.x / SPAWN_SIZE.x);
    public static int SPAWN_CELLS = SPAWN_ROWS * SPAWN_COLUMNS;

    public static Color COLOR_DAY = new Color32(0xFF, 0xFF, 0xFF, 0xFF);
    public static Color COLOR_SUNSET = new Color32(0xFF, 0x99, 0x66, 0xFF);
    public static Color COLOR_NIGHT = new Color32(0x66, 0x99, 0xcc, 0xFF);

    public static int SECONDS_IN_MINUTE = 60;
    public static int MINUTES_IN_HOUR = 60;
    public static int HOURS_IN_DAY = 24;
    public static int SECONDS_IN_HOUR = SECONDS_IN_MINUTE * MINUTES_IN_HOUR;
    public static int SECONDS_IN_DAY = SECONDS_IN_HOUR * HOURS_IN_DAY;
    public static int MINUTES_IN_DAY = MINUTES_IN_HOUR * HOURS_IN_DAY;

    public static int NUM_HOTEL_ROOMS = 10;
    public static int NUM_GARDENS = 10;
    public static int NUM_ROOM_SPOTS = 10;
    public static float GARDEN_START_DENSITY = 0.25f;
    public static int GARDEN_START_OBJECTS = (int)(SPAWN_CELLS * GARDEN_START_DENSITY);

    public static string[] GARDEN_NAMES = new string[] { "Forest", "Desert", "Spooky", "Tundra", "Volcano" };
    public static int[] GARDEN_UNLOCK_LEVELS = new int[] { 0, 1, 1, 1 };

    public static int NUM_COBWEBS = 4;
    public static int COBWEB_SPAWN_RADIUS = 4;
    public static float COBWEB_MIN_SCALE = 0.75f;
    public static float COBWEB_MAX_SCALE = 1f;

    public static Vector2 HOTEL_ROOM_GRID_SIZE = new Vector2(1, 1);

    public static float CUSTOMER_STEP_DELAY_MIN = 1f;
    public static float CUSTOMER_STEP_DELAY_MAX = 1f;
    public static float CUSTOMER_IDLE_DELAY_MIN = 1f;
    public static float CUSTOMER_IDLE_DELAY_MAX = 3f;
    public static float CUSTOMER_WALK_DELAY_MIN = 1f;
    public static float CUSTOMER_WALK_DELAY_MAX = 3f;
    public static float CUSTOMER_ADMIRE_DELAY_MIN = 3f;
    public static float CUSTOMER_ADMIRE_DELAY_MAX = 3f;

    public static float DEFAULT_SHAKE_TIME = 0.25F;
    public static float DEFAULT_SHAKE_FORCE = 1F;

    public static List<HotelRoomController> hotelRooms;
    public static Dictionary<string, HotelRoomSpotController> hotelRoomSpots;
    public static Dictionary<string, FurnitureController> furniture;
    public static Dictionary<string, MaterialController> materials;
    public static Dictionary<string, AnimalController> animals;
    public static Dictionary<string, HotelRoomController> rooms;

    public static Dictionary<string, RuntimeAnimatorController> animatorsMecanim;
    public static Dictionary<string, Sprite> sprites;

    public static Constants self;

    public Transform prefabsContainer;
    public MaterialController prefabMaterial;
    public FurnitureController prefabFurniture;
    public AnimalController prefabAnimal;
    public HotelRoomController prefabHotelRoom;
    public HotelRoomSpotController prefabHotelRoomSpot;
    public TextAsset textMaterialsList;
    public TextAsset textFurnitureList;
    public TextAsset textAnimalsList;
    public TextAsset textHotelRoomsList;
    public TextAsset textHotelRoomSpotsList;

    void Awake()
    {
        self = this;

        furniture = new Dictionary<string, FurnitureController>();
        materials = new Dictionary<string, MaterialController>();
        animals = new Dictionary<string, AnimalController>();
        rooms = new Dictionary<string, HotelRoomController>();
        hotelRooms = new List<HotelRoomController>();
        hotelRoomSpots = new Dictionary<string, HotelRoomSpotController>();

        GameObject[] prefabs = Resources.LoadAll<GameObject>(PREFAB_PATH);

        for (int i = 0; i < prefabs.Length; i++)
        {
            GameObject prefab = prefabs[i];

            FurnitureController furnitureController = prefab.GetComponent<FurnitureController>();
            MaterialController materialController = prefab.GetComponent<MaterialController>();
            AnimalController animalController = prefab.GetComponent<AnimalController>();
            HotelRoomController hotelRoomController = prefab.GetComponent<HotelRoomController>();
            HotelRoomSpotController hotelRoomSpotController = prefab.GetComponent<HotelRoomSpotController>();

            if (furnitureController != null)
            {
                furniture.Add(furnitureController.resourceTag, furnitureController);
            }

            if (materialController != null)
            {
                materials.Add(materialController.resourceTag, materialController);
            }

            if (animalController != null)
            {
                animals.Add(animalController.resourceTag, animalController);
            }

            if (hotelRoomController != null)
            {
                rooms.Add(hotelRoomController.resourceTag, hotelRoomController);
                hotelRooms.Add(hotelRoomController);
            }

            if (hotelRoomSpotController != null)
            {
                hotelRoomSpots.Add(hotelRoomSpotController.resourceTag, hotelRoomSpotController);
            }

        }

        hotelRooms.OrderBy(x => x.index);

    }

    public static FurnitureController getFurnitureController(string name)
    {
        if (!furniture.ContainsKey(name)) return null;
        return furniture[name];
    }

    public static MaterialController getMaterialController(string name)
    {
        if (!materials.ContainsKey(name)) return null;
        return materials[name];
    }

    public static AnimalController getAnimalController(string name)
    {
        if (!animals.ContainsKey(name)) return null;
        return animals[name];
    }

    public static RuntimeAnimatorController getAnimator(string name)
    {
        if (!animatorsMecanim.ContainsKey(name)) return null;
        return animatorsMecanim[name];
    }

    public static Sprite getSprite(string name)
    {
        if (!sprites.ContainsKey(name)) return null;
        return sprites[name];
    }

    public static int getGardenExperience(int level)
    {
        return (int)(100f * (1f + (float)level / 10f));
    }

    public static int getHotelExperience(int level)
    {
        return (int)(100f * (1f + (float)level / 10f));
    }

    public static int getFurnitureBuildTime(int hotel, int spot, int level)
    {
        return (1 + level) * 60;
    }

    public static int getHotelRoomBuildTime(int hotel)
    {
        return (1 + hotel) * 60;
    }

    private void spawnFromFilesDeprecated()
    {
        int i;

        animatorsMecanim = new Dictionary<string, RuntimeAnimatorController>();
        RuntimeAnimatorController[] animatorsMecanimList = Resources.LoadAll<RuntimeAnimatorController>("animations");
        foreach (RuntimeAnimatorController rac in animatorsMecanimList)
        {
            animatorsMecanim[rac.name] = rac;
        }

        sprites = new Dictionary<string, Sprite>();
        Sprite[] spritesPlaceholders = Resources.LoadAll<Sprite>("graphics/placeholders");
        foreach (Sprite s in spritesPlaceholders)
        {
            sprites[s.name] = s;
        }
        Sprite[] spritesOfficial = Resources.LoadAll<Sprite>("graphics/official");
        foreach (Sprite s in spritesOfficial)
        {
            sprites[s.name] = s;
        }

        materials = new Dictionary<string, MaterialController>();
        string[] materialsLines = Utils.splitLines(textMaterialsList.text);
        foreach (string line in materialsLines)
        {
            if (line.Length > 0 && line[0] != '#')
            {
                string[] parameters = Utils.splitAndTrim(line, '\t');
                if (parameters.Length >= 13)
                {
                    string type = parameters[0].ToLower();
                    string tag = parameters[1].ToLower();
                    string name = parameters[2];
                    string description = parameters[3];
                    string spriteName = parameters[4];
                    string animationSprite = parameters[5];
                    string animator = parameters[6];
                    string amount = parameters[7];
                    string spawnOnClick = parameters[8];
                    string offset = parameters[9];
                    string scale = parameters[10];
                    string experience = parameters[11];
                    string canCollectOnHover = parameters[12].ToLower();

                    MaterialController newMaterial = Instantiate<MaterialController>(prefabMaterial);
                    newMaterial.name = tag;
                    newMaterial.transform.SetParent(prefabsContainer);
                    newMaterial.transform.localPosition = Vector3.zero;

                    if (type.Equals("material")) newMaterial.type = MaterialController.TYPE.MATERIAL;
                    else if (type.Equals("currency")) newMaterial.type = MaterialController.TYPE.CURRENCY;
                    else if (type.Equals("spawnable")) newMaterial.type = MaterialController.TYPE.SPAWNABLE;

                    newMaterial.resourceTag = tag;
                    newMaterial.resourceName = name;
                    newMaterial.description = description;
                    newMaterial.canCollectOnHover = canCollectOnHover.Equals("true") || canCollectOnHover.Equals("1");

                    if (sprites.ContainsKey(spriteName)) newMaterial.sprite = sprites[spriteName];

                    int amountInt = 0;
                    try
                    {
                        amountInt = int.Parse(amount);
                    }
                    catch { }
                    newMaterial.amount = amountInt;

                    int experienceInt = 0;
                    try
                    {
                        experienceInt = int.Parse(experience);
                    }
                    catch { }
                    newMaterial.experience = experienceInt;

                    string[] spawnObjects = Utils.splitAndTrim(spawnOnClick, ',');
                    newMaterial.objectSpawnDefitions = new SpawnDefinition[spawnObjects.Length];
                    for (i = 0; i < spawnObjects.Length; i++)
                    {
                        string spawnObject = spawnObjects[i];
                        string[] spawnParameters = Utils.splitAndTrim(spawnObject, '*');
                        if (spawnParameters.Length >= 2)
                        {
                            string spawnTag = spawnParameters[0];
                            int spawnProbability = 0;
                            try
                            {
                                spawnProbability = int.Parse(spawnParameters[1]);
                            }
                            catch { }
                            newMaterial.objectSpawnDefitions[i] = new SpawnDefinition();
                            newMaterial.objectSpawnDefitions[i].tag = spawnTag;
                            newMaterial.objectSpawnDefitions[i].spawnProbability = spawnProbability;
                        }
                    }

                    AnimationController animationController = newMaterial.GetComponent<AnimationController>();
                    if (animatorsMecanim.ContainsKey(animator)) animationController.setAnimator(animatorsMecanim[animator]);

                    string[] offsetValues = Utils.splitAndTrim(offset, ',');
                    if (offsetValues.Length >= 2)
                    {
                        float offsetX = 0;
                        float offsetY = 0;
                        try
                        {
                            offsetX = float.Parse(offsetValues[0]);
                            offsetY = float.Parse(offsetValues[1]);
                        }
                        catch { }
                        newMaterial.getCollider().offset = new Vector2(offsetX, offsetY);
                    }

                    if (animationSprite.Length > 0 && sprites.ContainsKey(animationSprite)) animationController.spriteRenderer.sprite = sprites[animationSprite];

                    string[] scaleValues = Utils.splitAndTrim(scale, ',');
                    if (scaleValues.Length >= 2)
                    {
                        float radius = 0;
                        try
                        {
                            radius = float.Parse(scaleValues[0]);
                        }
                        catch { }
                        newMaterial.getCollider().radius = radius;
                    }

                    materials[tag] = newMaterial;
                }
            }
        }

        furniture = new Dictionary<string, FurnitureController>();
        string[] furnitureLines = Utils.splitLines(textFurnitureList.text);
        foreach (string line in furnitureLines)
        {
            if (line.Length > 0 && line[0] != '#')
            {
                string[] parameters = Utils.splitAndTrim(line, '\t');
                if (parameters.Length >= 7)
                {
                    string tag = parameters[0].ToLower();
                    string name = parameters[1];
                    string description = parameters[2];
                    string spriteName = parameters[3];
                    string recipe = parameters[4];
                    string currency = parameters[5];
                    string experience = parameters[6];

                    FurnitureController newFurniture = Instantiate<FurnitureController>(prefabFurniture);
                    newFurniture.name = tag;
                    newFurniture.transform.SetParent(prefabsContainer);
                    newFurniture.transform.localPosition = Vector3.zero;

                    newFurniture.resourceTag = tag;
                    newFurniture.resourceName = name;
                    newFurniture.description = description;

                    if (sprites.ContainsKey(spriteName)) newFurniture.sprite = sprites[spriteName];

                    string[] recipeValues = Utils.splitAndTrim(recipe, ',');
                    newFurniture.recipe = new RecipeIngredient[recipeValues.Length];
                    for (i = 0; i < recipeValues.Length; i++)
                    {
                        string[] ingredientValues = Utils.splitAndTrim(recipeValues[i], '*');
                        string ingredientTag = ingredientValues[0];
                        int ingredientAmount = 0;
                        if (ingredientValues.Length >= 2)
                        {
                            try
                            {
                                ingredientAmount = int.Parse(ingredientValues[1]);
                            }
                            catch { }
                        }
                        newFurniture.recipe[i] = new RecipeIngredient(ingredientTag, ingredientAmount);
                    }

                    newFurniture.currencyRequired = 0;
                    try
                    {
                        newFurniture.currencyRequired = int.Parse(currency);
                    }
                    catch { }

                    newFurniture.experience = 0;
                    try
                    {
                        newFurniture.experience = int.Parse(experience);
                    }
                    catch { }

                    furniture[tag] = newFurniture;
                }
            }
        }

        hotelRoomSpots = new Dictionary<string, HotelRoomSpotController>();
        string[] spotsLines = Utils.splitLines(textHotelRoomSpotsList.text);
        foreach (string line in spotsLines)
        {
            if (line.Length > 0 && line[0] != '#')
            {
                string[] parameters = Utils.splitAndTrim(line, '\t');
                if (parameters.Length >= 6)
                {
                    string spotResourceTag = parameters[0].ToLower();
                    string spotResourceName = parameters[1];
                    string spotDescription = parameters[2];
                    string spotPosition = parameters[3];
                    string spotCollider = parameters[4];
                    string spotFurniture = parameters[5].ToLower();

                    HotelRoomSpotController newSpot = Instantiate<HotelRoomSpotController>(prefabHotelRoomSpot);
                    newSpot.name = spotResourceTag;
                    newSpot.transform.SetParent(prefabsContainer);
                    newSpot.transform.localPosition = Vector3.zero;

                    newSpot.resourceTag = spotResourceTag;
                    newSpot.resourceName = spotResourceName;
                    newSpot.description = spotDescription;

                    newSpot.roomPosition = new Vector2(0, 0);
                    string[] positionValues = Utils.splitAndTrim(spotPosition, ',');
                    if (positionValues.Length >= 2)
                    {
                        try
                        {
                            newSpot.roomPosition.x = float.Parse(positionValues[0]);
                            newSpot.roomPosition.y = float.Parse(positionValues[1]);
                        }
                        catch { }
                    }

                    newSpot.colliderSize = new Vector2(0, 0);
                    newSpot.colliderOffset = new Vector2(0, 0);
                    string[] colliderValues = Utils.splitAndTrim(spotCollider, ',');
                    if (colliderValues.Length >= 4)
                    {
                        try
                        {
                            newSpot.colliderOffset.x = float.Parse(colliderValues[0]);
                            newSpot.colliderOffset.y = float.Parse(colliderValues[1]);
                            newSpot.colliderSize.x = float.Parse(colliderValues[2]);
                            newSpot.colliderSize.y = float.Parse(colliderValues[3]);
                        }
                        catch { }
                    }

                    newSpot.furniture = Utils.splitAndTrim(spotFurniture, ',');

                    hotelRoomSpots[spotResourceTag] = newSpot;
                }
            }
        }

        hotelRooms = new List<HotelRoomController>();
        string[] hotelLines = Utils.splitLines(textHotelRoomsList.text);
        foreach (string line in hotelLines)
        {
            if (line.Length > 0 && line[0] != '#')
            {
                string[] parameters = Utils.splitAndTrim(line, '\t');
                if (parameters.Length >= 8)
                {
                    string hotelResourceTag = parameters[0].ToLower();
                    string hotelResourceName = parameters[1];
                    string hotelDescription = parameters[2];
                    string hotelRecipe = parameters[3].ToLower();
                    string hotelSpots = parameters[4].ToLower();
                    string hotelBackground = parameters[5];
                    string hotelMask = parameters[6];
                    string hotelCurrency = parameters[7];

                    HotelRoomController newHotel = Instantiate<HotelRoomController>(prefabHotelRoom);
                    newHotel.name = hotelResourceTag;
                    newHotel.transform.SetParent(prefabsContainer);
                    newHotel.transform.localPosition = Vector3.zero;

                    newHotel.resourceTag = hotelResourceTag;
                    newHotel.resourceName = hotelResourceName;
                    newHotel.description = hotelDescription;
                    newHotel.background = hotelBackground;

                    string[] recipeValues = Utils.splitAndTrim(hotelRecipe, ',');
                    newHotel.recipe = new RecipeIngredient[recipeValues.Length];
                    for (i = 0; i < recipeValues.Length; i++)
                    {
                        string[] ingredientValues = Utils.splitAndTrim(recipeValues[i], '*');
                        string ingredientTag = ingredientValues[0];
                        int ingredientAmount = 0;
                        if (ingredientValues.Length >= 2)
                        {
                            try
                            {
                                ingredientAmount = int.Parse(ingredientValues[1]);
                            }
                            catch { }
                        }
                        newHotel.recipe[i] = new RecipeIngredient(ingredientTag, ingredientAmount);
                    }

                    newHotel.spots = Utils.splitAndTrim(hotelSpots, ',');
                    for (i = 0; i < newHotel.spots.Length; i++)
                    {
                        if (hotelRoomSpots.ContainsKey(newHotel.spots[i])) hotelRoomSpots[newHotel.spots[i]].index = i;
                    }

                    newHotel.maskSize = new Vector2(0, 0);
                    newHotel.maskOffset = new Vector2(0, 0);
                    string[] colliderValues = Utils.splitAndTrim(hotelMask, ',');
                    if (colliderValues.Length >= 4)
                    {
                        try
                        {
                            newHotel.maskOffset.x = float.Parse(colliderValues[0]);
                            newHotel.maskOffset.y = float.Parse(colliderValues[1]);
                            newHotel.maskSize.x = float.Parse(colliderValues[2]);
                            newHotel.maskSize.y = float.Parse(colliderValues[3]);
                        }
                        catch { }
                    }

                    newHotel.currencyRequired = 0;
                    try
                    {
                        newHotel.currencyRequired = int.Parse(hotelCurrency);
                    }
                    catch { }

                    hotelRooms.Add(newHotel);
                }
            }
        }

        animals = new Dictionary<string, AnimalController>();
        string[] animalsLines = Utils.splitLines(textAnimalsList.text);
        foreach (string line in animalsLines)
        {
            if (line.Length > 0 && line[0] != '#')
            {
                string[] parameters = Utils.splitAndTrim(line, '\t');
                if (parameters.Length >= 5)
                {
                    string tag = parameters[0].ToLower();
                    string animator = parameters[1];
                    string produce = parameters[2];
                    string poop = parameters[3];
                    string offset = parameters[4];
                    string scale = parameters[5];

                    AnimalController newAnimal = Instantiate<AnimalController>(prefabAnimal);
                    newAnimal.name = tag;
                    newAnimal.transform.SetParent(prefabsContainer);
                    newAnimal.transform.localPosition = Vector3.zero;

                    newAnimal.resourceTag = tag;
                    newAnimal.produceTag = produce;
                    newAnimal.poopTag = poop;

                    AnimationController animationController = newAnimal.GetComponent<AnimationController>();
                    if (animatorsMecanim.ContainsKey(animator)) animationController.setAnimator(animatorsMecanim[animator]);

                    string[] offsetValues = Utils.splitAndTrim(offset, ',');
                    if (offsetValues.Length >= 2)
                    {
                        float offsetX = 0;
                        float offsetY = 0;
                        try
                        {
                            offsetX = float.Parse(offsetValues[0]);
                            offsetY = float.Parse(offsetValues[1]);
                        }
                        catch { }
                        newAnimal.getCollider().offset = new Vector2(offsetX, offsetY);
                    }

                    string[] scaleValues = Utils.splitAndTrim(scale, ',');
                    if (scaleValues.Length >= 1)
                    {
                        float radius = 0;
                        try
                        {
                            radius = float.Parse(scaleValues[0]);
                        }
                        catch { }
                        newAnimal.getCollider().radius = radius;
                    }

                    animals[tag] = newAnimal;
                }
            }
        }
    }
}
