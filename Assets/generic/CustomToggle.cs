﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CustomToggle : MonoBehaviour
{
    public GameObject checkmark;
    public Text label;
    
    public CustomToggleGroup group;

    private Button button;

    void Awake ()
    {
        button = GetComponent<Button>();
        if (group != null) group.addToggle(this);
    }
    
    void Update ()
    {
        
    }

    public void setStatus(bool isOn)
    {
        if (group != null)
        {
            if (isOn)
            {
                foreach (CustomToggle toggle in group.getToggles())
                {
                    if (toggle != null) toggle.checkmark.SetActive(false);
                }
            }
        }
        checkmark.SetActive(isOn);
    }

    public bool getStatus()
    {
        return checkmark.activeSelf;
    }

    public void setInteractable(bool interactable)
    {
        button.interactable = interactable;
    }

    public bool getInteractable()
    {
        return button.interactable;
    }
}
