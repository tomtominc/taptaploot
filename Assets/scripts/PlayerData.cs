﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    public bool needHelpShown;
    public int language;
    public int currency;
    public int premiumCurrency;
    public int lastGarden;
    public int lastHotelRoom;
    public int hotelExperience;
    public int hotelLevel;
    public List<PlayerDataGarden> gardens;
    public List<PlayerDataHotelRoom> hotelrooms;
    public Dictionary<string, PlayerDataInventoryItem> inventoryItems;
}