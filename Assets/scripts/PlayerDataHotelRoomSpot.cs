﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerDataHotelRoomSpot
{
    public int level;
    public System.DateTime buildTime;
    public bool isBuilding;
}
