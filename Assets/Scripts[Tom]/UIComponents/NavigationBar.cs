﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationBar : MonoBehaviour
{
    public List<NavigationTab> tabs;

    public void OnEnable()
    {
        for (int i = 0; i < tabs.Count; i++)
        {
            tabs[i].clickedTabAction += OnClickedTab;
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < tabs.Count; i++)
        {
            tabs[i].clickedTabAction -= OnClickedTab;
        }
    }

    public void showNotificationIcon(int p_tab)
    {
        tabs[p_tab].showNotificationBadge();
    }

    public void hideNotificationIcon(int p_tab)
    {
        tabs[p_tab].showNotificationBadge();
    }

    private void OnClickedTab(int p_tabIndex)
    {
        tabs[p_tabIndex].hideNotificationBadge();


        switch (p_tabIndex)
        {
            case 0: UIMainController.self.onClickHotel(); break;
            case 1: UIMainController.self.onClickMap(); break;
            case 2: UIMainController.self.onClickInventory(); break;
        }
    }
}
