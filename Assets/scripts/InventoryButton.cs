﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour
{
    public Text textCount;
    public Image imageResource;
    
    private PlayerDataInventoryItem item;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void setItem(PlayerDataInventoryItem i)
    {
        item = i;
        textCount.text = i.type == PlayerDataInventoryItem.TYPE.BLUEPRINT ? "" : item.count.ToString();
        switch (i.type)
        {
        case PlayerDataInventoryItem.TYPE.MATERIAL: imageResource.sprite = Constants.materials[i.tag].sprite; break;
        case PlayerDataInventoryItem.TYPE.FURNITURE: imageResource.sprite = Constants.furniture[i.tag].sprite; break;
        }
    }

    public void onClick()
    {
        switch (item.type)
        {
        case PlayerDataInventoryItem.TYPE.MATERIAL:
            UIMainController.self.setSelectedInventoryItem(item);
            break;
        case PlayerDataInventoryItem.TYPE.FURNITURE:
            UIMainController.self.setSelectedFurnitureItem(item);
            break;
        }
    }
}
