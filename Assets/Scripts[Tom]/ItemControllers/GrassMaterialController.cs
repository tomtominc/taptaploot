﻿using System.Collections.Generic;
using UnityEngine;

public class GrassMaterialController : MaterialController
{
    [SerializeField]
    private List<Sprite> grassSpriteByGardenId;

    public override void onSpawned()
    {
        int gardenIndex = GameMainController.self.getCurrentGardenIndex();

        if (gardenIndex < grassSpriteByGardenId.Count)
        {
            SpriteRenderer grassRenderer = GetComponentInChildren<SpriteRenderer>();
            sprite = grassSpriteByGardenId[gardenIndex];
            grassRenderer.sprite = sprite;
        }
    }
}
